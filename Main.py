"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
import sys
import os.path
import soundcard
from matplotlib import pyplot
from program.Home import Home_widget
from program.Perception import Perception_widget
from program.EGG import EGG_widget
from program.Aerodynamics import Aerodynamics_widget
from program.Acoustics import Acoustics_widget
from program.VDI import VDI_widget
import program.Data_handler as Data_handler

class Main_window(QtWidgets.QMainWindow):
   
    signal_speaker = QtCore.Signal(str)
    signal_mic = QtCore.Signal(str)

    def __init__(self):
        """
        Constructor of class 'Main_Window'

        Creates object of class "Tab-widget" and sets it as the 
        central Widget. Sets all buttons and menus in Main Window.
        Connects buttons and menus with functions in class "Data_handler". 

        Called upon in main function.
        """
        super().__init__()

        # create central widget
        self.setWindowTitle("Voice DiagnostEGG")
        self.setWindowIcon(QtGui.QIcon("program\\icons\\speech_bubble.png"))
        tab_widget = Tab_widget()
        self.setCentralWidget(tab_widget)
        screen = app.primaryScreen()
        height = 0.5 * screen.size().width()
        width = 1.5 * height
        self.setMinimumSize(width, height)        

        # create drop-down menus 
        menu = self.menuBar()

        # menu to load signal or record signal    
        file_menu = QtWidgets.QMenu("Datei")
        load_file_action = file_menu.addAction("Datei laden")
        load_file_action.triggered.connect(Data_handler.load_file)
        record_audio_action = file_menu.addAction("Datei aufnehmen")
        record_audio_action.triggered.connect(Data_handler.open_recording_window)
        save_data_action = file_menu.addAction("Messung speichern")
        save_data_action.triggered.connect(Data_handler.save_all_data_to_file)
        menu.addMenu(file_menu)   

        # menu to choose audio devices
        audio_device_menu = self.get_audio_device_menu()
        menu.addMenu(audio_device_menu)     

        # menu to display information
        help_menu = QtWidgets.QMenu("Hilfe")
        perception_action = help_menu.addAction("RBH-Skala")
        perception_action.triggered.connect(self.help_perception)
        egg_action = help_menu.addAction("Elektroglottographie")
        egg_action.triggered.connect(self.help_EGG)
        aerodynamics_action = help_menu.addAction("Aerodynamik")
        aerodynamics_action.triggered.connect(self.help_aerodynamics)
        acoustics_action = help_menu.addAction("Akustik")
        acoustics_action.triggered.connect(self.help_acoustics)
        vdi_action = help_menu.addAction("Stimmstörungsindex")
        vdi_action.triggered.connect(self.help_VDI)
        menu.addMenu(help_menu)

        # signals connected to Data_Handler
        self.signal_speaker.connect(Data_handler.receive_speaker_name)
        self.signal_mic.connect(Data_handler.receive_mic_name)

    def help_perception(self):
        """
        Sets title, text and informative text in Messagebox
        in help menu "RBH-Skala".

        Called upon in Constructor of class "Main_window".
        """
        title = "RBH-Skala"
        text = "Informationen zur RBH-Skala"
        informative_text = ("Die RBH-Skala dient der subjektiven Bewertung der Stimme. "
                                "Dabei werden die Aspekte Rauigkeit (R), Behauchtheit (B) "
                                "und Heiserkeit (H) beurteilt. Heiserkeit beschreibt die "
                                "allgemeine Stimmqualität. Bei der Behauchheit wird der "
                                "Höreindruck von turbulenten Luftausströmungen beurteilt "
                                "und bei der Rauigkeit wird der Höreindruck unregelmäßgier "
                                "Glottispulse bzw. einer Änderung der Grundfrequenz beschrieben."
                                "Die Beurteilung erfolgt auf einer Skala von 0 bis 3, wobei 0 "
                                "'normal' und 3 'hochgradig' entspricht.")
        self.open_message_box(title, text, informative_text)
    
    def help_EGG(self):
        """
        Sets title, text and informative text in Messagebox
        in help menu "Elektroglottographie".

        Called upon in Constructor of class "Main_window".
        """
        title = "Elektroglottographie"
        text = "Informationen zur Elektroglottographie"
        informative_text = ("Bei der Elektroglottographie handelt es sich um eine Analyse "
                                "der Stimmlippenschwingung. Für die Messung wird ein Laryngograph "
                                "verwendet. Über zwei Elektroden, welche seitlich des Schild"
                                "knorpels angebracht werden, werden Widerstandsänderungen gemessen. "
                                "Zusätzlich wird mit einem Mikrofon der Schalldruckpegel aufgenommen. "
                                "Anhand der Widerstandsänderungen kann die Kontakt- und Öffnungs"
                                "phase ermittelt und weitere Analysen vorgenommen werden.")
        self.open_message_box(title, text, informative_text)

    def help_aerodynamics(self):
        """
        Sets title, text and informative text in Messagebox
        in help menu "Aerodynamik".

        Called upon in Constructor of class "Main_window".
        """
        title = "Aerodynamik"
        text = "Informationen zur Aerodynamik"
        informative_text = ("Innerhalb der aerodynamischen Untersuchung wird die Vitalkapazität (VC) "
                                "und die maximale Tonhaltedauer (MPT) untersucht. Das Verhältnis "
                                "dieser beiden Größen ergibt den Phonationsquotient (PQ). "
                                "Mithilfe einer Spirometrie oder einer Peak-Flow Messung wird die "
                                "Vitalkapazität bewertet. Für die maximale Tonhaltedauer wird mit einer "
                                "Stoppuhr gemessen, wie lange der Vokal /a:/ nach vollständiger "
                                "Inspiration gehalten werden kann.")
        self.open_message_box(title, text, informative_text)

    def help_acoustics(self):
        """
        Sets title, text and informative text in Messagebox
        in help menu "Akustik".

        Called upon in Constructor of class "Main_window".
        """
        title = "Akustik"
        text = "Informationen zur Akustik"
        informative_text = ("Bei der akustischen Analyse wird die Stimme objektiv eingeschätzt."
                                "Zunächst wird ein Spektrogramm erstellt. Für die weitere Auswertung "
                                "wird ein lang anhaltender Vokal /a:/ benötigt. "
                                "Analysiert werden Jitter , d.h. die Perioditätsvariationen der "
                                "Periodendauer, und Shimmer (Norm: 0.5 - 4%), d.h. die Periodiätsvariationen der "
                                "Amplituden. Zusätzlich wird die Grundfrequenz F0 der Stimme "
                                "berechnet.")
        self.open_message_box(title, text, informative_text)
        
    def help_VDI(self):
        """
        Sets title, text and informative text in Messagebox
        in help menu "Stimmstörungsindex".

        Called upon in Constructor of class "Main_window".
        """
        title = "Stimmstörungsindex"
        text = "Informationen zum Stimmstörungsindex"
        informative_text = ("Der Stimmstörungsindex ist ein Fragebogen zur subjektiven "
                                "Einschätzung der Stimme im Alltag. Zwölf Fragen aus den "
                                "Teilbereichen 'funktional', 'physisch' und 'emotional' "
                                "werden auf einer Skala von 0 bis 4 bewertet und es gilt \n"
                                "0: nie, 1: selten, 2: manchmal, 3: oft, 4: immer.")
        self.open_message_box(title, text, informative_text)

    def open_message_box(self, title, text, informative_text):
        """
        Creates messagbox to display information asked from 
        the help menu.

        Called upon in functions "help_perception", "help_EGG",
        "help_aerodynamics", "help_acoustics", "help_VDI".
        """
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIcon(QtWidgets.QMessageBox.Information)
        msg_box.setWindowTitle(title)
        msg_box.setText(text)
        msg_box.setInformativeText(informative_text)
        msg_box.exec_()

    def get_audio_device_menu(self):
        """
        Gets all speaker and microphone devices which are currently connected 
        and creates a drop down menu "Audiogeräte" where speakers and microphones
        are listed. Sends the clicked speaker / microphone as a signal.
        Code used from: https://pypi.org/project/SoundCard/ (Bastian Bechtold)

        Return:
        -------
        audio_device_menu
                QMenu with all speaker and microphone devices which 
                are currently connected.

        Called upon in constructor of class "Main_window".
        """
        # connected speakers
        all_speakers = soundcard.all_speakers()
        speaker_menu = QtWidgets.QMenu("Lautsprecher")
        self.speaker_action_group = QtWidgets.QActionGroup(self)
        for speaker in all_speakers:
            speaker_action = speaker_menu.addAction(speaker.name)
            speaker_action.setCheckable(True)
            self.speaker_action_group.addAction(speaker_action)
        self.speaker_action_group.triggered.connect(self.emit_speaker_signal)
        
        # connected microphones
        all_mics = soundcard.all_microphones()
        mic_menu = QtWidgets.QMenu("Mikrofon")
        self.mic_action_group = QtWidgets.QActionGroup(self)
        for mic in all_mics:
            mic_action = mic_menu.addAction(mic.name)
            mic_action.setCheckable(True)
            self.mic_action_group.addAction(mic_action)
        self.mic_action_group.triggered.connect(self.emit_mic_signal)

        audio_device_menu = QtWidgets.QMenu("Audiogeräte")
        speaker_action = audio_device_menu.addMenu(speaker_menu)
        mic_action = audio_device_menu.addMenu(mic_menu)

        return audio_device_menu

    def emit_speaker_signal(self):
        """
        Emits the signal for the clicked speaker.

        Called upon in the function "get_audio_device"
        """
        checked_action = self.speaker_action_group.checkedAction()
        speaker_name = checked_action.text()
        self.signal_speaker.emit(speaker_name)

    def emit_mic_signal(self):
        """
        Emits the signal for the clicked microphone.

        Called upon in the function "get_audio_device"
        """
        checked_action = self.mic_action_group.checkedAction()
        mic_name = checked_action.text()
        self.signal_mic.emit(mic_name)

    def closeEvent(self, close_event):
        """
        Defines the action when main window is closed.

        Accepts closing event, when all data is saved. Creates a 
        messagebox, when data was not saved before and asks user
        to save, not save or cancel the action.

        Parameter:
        ----------
        close_event
                Event object containing information about event
                'closeEvent'
        """
        if Data_handler.session_saved == True:
            close_event.accept()
        else:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setIcon(QtWidgets.QMessageBox.Warning)
            msg_box.setWindowTitle("Schließen ohne zu speichern?")
            msg_box.setText("Wenn Sie das Programm schließen ohne zu speichern, gehen aktuelle Änderungen verloren.")
            save_button = QtWidgets.QPushButton("Speichern")
            msg_box.addButton(save_button, QtWidgets.QMessageBox.ActionRole)
            not_save_button = QtWidgets.QPushButton("Nicht speichern")
            msg_box.addButton(not_save_button, QtWidgets.QMessageBox.ActionRole)
            cancel_button = QtWidgets.QPushButton("Abbrechen")
            msg_box.addButton(cancel_button, QtWidgets.QMessageBox.ActionRole)
            
            save_button.clicked.connect(self.save_upon_closing)
            not_save_button.clicked.connect(self.do_not_save_upon_closing)
            cancel_button.clicked.connect(close_event.ignore)

            msg_box.exec_()            
            
    def save_upon_closing(self):
        """
        Saves all data with function "save_all_data_to_file" from
        "Data_handler" when button "Speichern" is clicked.

        Called upon in function "CloseEvent".
        """
        Data_handler.save_all_data_to_file()
        self.close()
    
    def do_not_save_upon_closing(self):
        """
        Closes MainWindow when button "Nicht speichern" is clicked.

        Called upon in function "CloseEvent".
        """
        self.close()


class Tab_widget(QtWidgets.QTabWidget):
    def __init__(self):
        """
        Constructor of class "Tab_widget".

        Creates all widgets for tabs "Home", "RBH-Skala",
        "Elektroglottographie", "Aerodynamik", "Akustik", "Stimmstörungsindex".
        Disables all tabs except "Home", enables tabs, when patient info 
        is received. When data in tabs is saved, the signals are connected with 
        "Data_handler to receive results from all the tabs.

        Called upon in the constructor of class "Main_window". 
        """
        super().__init__()

        self.setTabPosition(self.North)
        self.setTabShape(self.Triangular)

        # create widgets and add to central widget
        self.home_tab = Home_widget()
        self.perception_tab = Perception_widget()
        self.egg_tab = EGG_widget()
        self.aerodynamics_tab = Aerodynamics_widget()
        self.acoustics_tab = Acoustics_widget()
        self.VDI_tab = VDI_widget()

        self.addTab(self.home_tab, "1 Home")
        self.addTab(self.perception_tab, "2 RBH-Skala")
        self.addTab(self.egg_tab, "3 Elektroglottographie")
        self.addTab(self.aerodynamics_tab, "4 Aerodynamik")
        self.addTab(self.acoustics_tab, "5 Akustik")
        self.addTab(self.VDI_tab, "6 Stimmstörungsindex")

        # disable all tabs except home_tab
        self.perception_tab.setEnabled(False)
        self.egg_tab.setEnabled(False)
        self.aerodynamics_tab.setEnabled(False)
        self.acoustics_tab.setEnabled(False)
        self.VDI_tab.setEnabled(False)

        # connections with Data_handler when signals are emitted
        self.home_tab.signal_saved.connect(Data_handler.receive_patient_info)
        self.home_tab.signal_saved.connect(self.enable_Tabs)
        self.home_tab.signal_anamnesis.connect(Data_handler.receive_anamnesis)
        self.perception_tab.signal_saved.connect(Data_handler.receive_perception_result)
        self.perception_tab.play_audio_button.clicked.connect(Data_handler.play_audio)
        self.egg_tab.signal_saved.connect(Data_handler.receive_egg_result)
        self.aerodynamics_tab.signal_saved.connect(Data_handler.receive_aerodynamics_result)
        self.acoustics_tab.signal_saved.connect(Data_handler.receive_acoustics_result)
        self.VDI_tab.signal_saved.connect(Data_handler.receive_VDI_result)

        self.tabBarClicked.connect(self.display_plots)
    
    @QtCore.Slot(str)
    def enable_Tabs(self):
        """
        Enables all tabs when patient information is received and signal
        is emitted.

        Called upon in constructor of class "Tab_widget".
        """
        self.perception_tab.setEnabled(True)
        self.egg_tab.setEnabled(True)
        self.aerodynamics_tab.setEnabled(True)
        self.acoustics_tab.setEnabled(True)
        self.VDI_tab.setEnabled(True)
    
    @QtCore.Slot(int)
    def display_plots(self, tab_number):
        """
        Displays the plots in tab "Elektroglottographie" und "Akustik"
        when tabs are enabled and clicked.

        Parameters:
        ----------
        tab_number
                (int) number of tab that has been clicked
        
        Called upon in constructor of class "Tab_widget".
        """
        if tab_number == 2 and self.egg_tab.isEnabled() == True:
            self.egg_tab.get_Lx()
        if tab_number == 4 and self.acoustics_tab.isEnabled() == True:
            self.acoustics_tab.show_plots()


if __name__ == "__main__":
    from bleak_winrt import _winrt
    _winrt.uninit_apartment()
    
    app = QtWidgets.QApplication(sys.argv)

    main_window = Main_window()
    main_window.show()

    sys.exit(app.exec_())