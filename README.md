Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

## Table of Contents
- [Table of Contents](#table-of-contents)
- [Project: Electroglottography (EGG)](#project-electroglottography-egg)
- [Voice DiagnostEGG - General Information](#voice-diagnostegg---general-information)
- [Installation](#installation)
- [General setup of equipment](#general-setup-of-equipment)
- [Usage](#usage)
  - [Example 1](#example-1)
  - [Example 2](#example-2)
  - [Example 3](#example-3)
- [License](#license)

## Project: Electroglottography (EGG)
This project is part of the module "Projekt 2" in the 6th semester of the 
study course Hörtechnik und Audiologie at the Jade Hochschule in 
Oldenburg. The aim of the project was to familiarize with the field of 
electroglottography / electrolaryngography and learn about the use of an
electroglottograph/laryngograph and how it works. The electroglottograph 
was provided by the university. The package included the laryngograph, 
the software "LingWaves", a microphone, a soundcard, two electrodes and 
all needed cables to connect the devices to the soundcard and soundcard 
to the computer. This software "Voice DiagnostEGG" originated from the rather
complicated usage of several other softwares such as "LingWaves" 
analysing EGG signals.

## Voice DiagnostEGG - General Information
In general, the program consists of six different tab menus. Before any 
analysis can be made a patient file must exist. In the starting tab menu
"Home" a patient file with name, birthday, sex and height can either be
created or can be chosen from the directory if a patient file already 
exists. Additionally, an anamnesis can be filled in for each session 
which is also going to be saved in the patient file. In the next step 
the EGG signal to be analysed is to be recorded or chosen from the 
directory. In menu "Audiogeräte" the connected speaker and microphone 
must be selected. After that, a signal can be recorded in "Datei aufnehmen"
or it can be selected in "Datei laden" in menu "Datei". When a signal is 
recorded, the recording is saved in the directory "wav-files". For how to 
set up the equipment for recording see chapter "General setup of 
equipment" below.

In tabs 2-6 the analysis of the EGG signal can be analysed according to 
the basic protocoll, see chapter "Usage" below. In tab "RBH-Skala" 
three different parameters can be filled in and the speech signal can be
played via the chosen speakers. By clicking the button "Speichern", this
analysis can be saved within the program. In tab "Elektroglottographie" 
the EGG signal is plotted and a time range which is going to be analysed
can be filled in. By clicking analyse the different EGG parameters are 
calculated and shown. By clicking the button "Speichern", this analysis 
can be saved within the program. Tab "Aerodynamik" can analyse aerodynamic
parameters by filling two given parameters, where the vital capacity needs
to be measured with a spirometer. By clicking the button "Speichern", this
analysis can be saved within the program. In tab "Akustik" the acoustic 
analysis can be done. The plot on the left shows the speech signal in time,
whereas the plot on the right shows the spectrogramm of the whole signal. 
A time range can be filled in from which the analysis is then made. By 
clicking the button "Speichern", this analysis can be saved within the 
program. Tab "Stimmstörungsindex" can analyse the subjective rating from a 
patient. The patient is asked all 12 questions and by clicking "Auswertung"
the answers are examined. By clicking the button "Speichern", this analysis 
can be saved within the program. In the menu "Hilfe" all the different 
analysis from the five tabs is explained in a short passage.

After going through each tab the analysis can be saved in menu "Datei" and
"Messung speichern". Now, all the results are saved in the patient file. 
When opening the file from the directory "patient info", the information 
from the "Home" menu and the results from all the sessions are shown

## Installation
This program was written and tested within Python 3.9.6 on Windows. The authors 
cannot guarantee the program to work properly with other versions or operating
systems. This program contains the files:
- *Main.py*
- *program/Data_handler.py*
- *program/Home.py*
- *program/Perception.py*
- *program/EGG.py*
- *program/Aerodynamics.py*
- *program/Acoustics.py*
- *program/VDI.py*

The file "Main.py" is in the main program directory, whereas all the other 
files are placed in the seperate directory "program".

Additionally the program contains:
- *requirements.txt*
- *README.md*

All the modules needed for this program can be installed by calling
'pip install -r requirements.txt'
in the directory where the program is saved.

When all necessary modules are installed, the file "Main.py" can be 
executed on the command line in the chosen directory via
'python Main.py'
or can be executed via F5 in a programming environment such as 
Visual Studio Code.

Notice, that electroglottograph, microphone and soundcard are
connected before executing the program (see part "General setup of
equipment).

## General setup of equipment
For recording an EGG and speech signal the equipment needs to be set up 
correctly. The electrodes need to be placed at the thyroid cartilage and
the cable is to be connected to the electroglottograph/laryngograph. 
Then the electroglottograph/laryngograph and the microphone are 
connected to the soundcard. Note, that the microphone is connected to 
channel 1 and the electrodes are connected to channel 2 of the soundcard. 
Make sure, that everything is connected before executing the program.

When recording, the LEDs on the soundcard indicating clipping need to be
checked. If the LEDs are blinking, the gain must be turned down and a new
recording should be made. The recording should be at least several hundred
milliseconds long to get a significant acoustic analysis.

## Usage
This program can be used to analyse a patients voice with a provided 
electroglottograph according to the protocoll of the European 
Laryngological Society (ELS). Patient information can be filled in
or selected from a patient file already created. 

EGG and speech signals can either be loaded if they already exist
in a wav-file via menu "Datei" and "Datei laden" or they can be 
recorded via menu "Datei" and "Datei aufnehmen". The 
electroglottograph and microphone should then be connected to the 
soundcard. For further information see the part "General setup of 
equipment". 

After selecting or recording a signal various analysis
according to the protocol can be made. If patient information in the 
"Home" tab is not filled in, all the other tabs are disabled and analysis 
can not be made.

### Example 1
A person wants to analyse somebodys subjective rating of their own voice.
In this case the patient information are needed to be filled in in the 
"Home" tab. By clicking on the tab "Stimmstörungsindex" a german version 
of the voice handicap index is shown. The index can be calculated 
by answering all 12 questions and clicking the button "Auswertung". The
result is shown on the display and can be saved in the patients info file
by clicking on the menu "Datei" and "Messung speichern". Alternatively, 
the program asks to save when closing the window.

### Example 2
A person wants to record an EGG signal and the corresponding speech signal.
In this case the patient information are needed to be filled in in the 
"Home" tab. By selecting the correct microphone in menu "Audiogeräte" -> 
"Lautsprecher", the used microphone for the recording is chosen. By 
clicking on "Datei aufnehmen" in the menu "Datei", a new window is shown. 
Within this menu, the person is able to start a recording with the button
"Aufnahme starten" and to stop a recording by clicking the button "Aufnahme
beenden". Note, that the electroglottograph and microphone is connected to 
the soundcard in channel 2 and channel 1 respectively. It is important to 
check if the small LEDs on the soundcard indicating clipping are blinking.
If so, then the gain needs to be turned down and a new recording must take 
place. After stopping the recording, the new window closes and a new 
wav-file is saved in the directory "wav-file".

### Example 3
A person wants to analyse a just recorded EGG signal. It is assumed, that 
the person did connect everything properly, checked for clipping and filled 
in the patient information. By clicking on the tab "Elektroglottographie" 
the  EGG signal (Lx signal) is shown. By typing in a start and stop time, 
this  range can be analysed. After clicking the button "Analyse", the 
parameters for this range are calculated. The fundamental frequency, 
fundamental period, open/closed phase and open/closed quotient are shown. 
By clicking the button "Zurücksetzen" a new time range can be analysed. 
By clicking the button "Speichern", the current analysis is saved within 
the program. The analysis from all tabs can be saved by clicking the button 
"Messung speichern" in menu "Datei".

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details:
<http://www.gnu.org/licenses/>