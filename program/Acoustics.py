"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
import parselmouth
import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Figure_canvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as Navigation_toolbar
from matplotlib.figure import Figure
import scipy.signal
import program.Data_handler as Data_handler

class Acoustics_widget(QtWidgets.QWidget):
    voice_signal = []
    signal_saved = QtCore.Signal(str)
    used_file_name = ""

    def __init__(self):
        """
        Constructor of class "Acoustics_widget".

        Sets a scene, creates two objects of a plot canvas and 
        navigation toolbars and sets all buttons and textfields.

        Called upon in constructor of class "Tab_widget" in "Main.py".        
        """
        QtWidgets.QWidget.__init__(self)

        # create main widget
        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        # create widgets to add into main widget
        self.canvas_voice = Plot_canvas(self)
        self.toolbar_voice = Navigation_toolbar(self.canvas_voice, self)
        self.canvas_spectrogram = Plot_canvas(self)
        self.toolbar_spectrogram = Navigation_toolbar(self.canvas_spectrogram, self)
        
        label_time_range = QtWidgets.QLabel("Zu analysierenden Zeitbereich auswählen: ")
        label_time_start = QtWidgets.QLabel("von (in sek): ")
        self.textfield_time_start = QtWidgets.QLineEdit("")
        label_time_end = QtWidgets.QLabel("bis (in sek): ")
        self.textfield_time_end = QtWidgets.QLineEdit("")

        self.button_analyse = QtWidgets.QPushButton("Analyse")
        self.button_analyse.clicked.connect(self.analyse)
        self.button_analyse.setEnabled(False)
        button_reset_plot = QtWidgets.QPushButton("Zurücksetzen")
        button_reset_plot.clicked.connect(self.reset_plots)

        self.label_fund_freq = QtWidgets.QLabel("Grundfrequenz F0: ")
        self.label_jitter = QtWidgets.QLabel("Jitter: ")
        self.label_shimmer = QtWidgets.QLabel("Shimmer: ")

        self.label_comment = QtWidgets.QLabel("Weitere Kommentare:")
        self.textfield_comment = QtWidgets.QLineEdit("")

        self.button_save = QtWidgets.QPushButton("Speichern")
        self.button_save.clicked.connect(self.save)
        self.button_save.setEnabled(False)
        self.label_saved = QtWidgets.QLabel("")

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.toolbar_voice, 1, 2, 1, 2)
        self.layout.addWidget(self.canvas_voice, 2, 1, 5, 3)
        self.layout.addWidget(self.toolbar_spectrogram, 1, 4, 1, 2)
        self.layout.addWidget(self.canvas_spectrogram, 2, 4, 5, 2)
        self.layout.addWidget(label_time_range, 7, 1)
        self.layout.addWidget(label_time_start, 7, 2)
        self.layout.addWidget(self.textfield_time_start, 7, 3)
        self.layout.addWidget(label_time_end, 7, 4)
        self.layout.addWidget(self.textfield_time_end, 7, 5)
        self.layout.addWidget(self.button_analyse, 8, 1)
        self.layout.addWidget(button_reset_plot, 8, 5)
        self.layout.addWidget(self.label_fund_freq, 9, 1, 1, 3)
        self.layout.addWidget(self.label_jitter, 10, 1, 1, 3)
        self.layout.addWidget(self.label_shimmer, 11, 1, 1, 3)
        self.layout.addWidget(self.label_comment, 12, 1)
        self.layout.addWidget(self.textfield_comment, 12, 2, 1, -1)
        self.layout.addWidget(self.button_save, 13, 1)
        self.layout.addWidget(self.label_saved, 13, 3, 1, -1)
        self.setLayout(self.layout)

    def show_plots(self):
        """
        Displays the plot of the speech signal in channel 1 in the
        chosen range from the textfields and shows the spectrogramm
        of the complete signal.

        Called upon in functions "analyse" and "reset_plot".
        """
        self.fs, signal, self.used_file_name = Data_handler.get_signal()
        if signal != []:

            # signal from channel 1
            channel_1 = signal[:,0]

            # create time vector
            time_vec = np.linspace(0, len(channel_1)-1, len(channel_1))
            time_vec = time_vec/self.fs
            
            # get time range to be analysed
            start_time = self.textfield_time_start.text()
            stop_time = self.textfield_time_end.text()
            if "," in start_time:
                start_time = start_time.replace(",", ".")
            if "," in stop_time:
                stop_time = stop_time.replace(",", ".")

            start_idx = 0
            stop_idx = len(channel_1)-1

            if start_time != "" and stop_time != "" and start_time < stop_time:             
                start_time = float(start_time)
                stop_time = float(stop_time)
                
                for idx, time in enumerate(time_vec):
                    if time >= start_time:
                        start_idx = idx
                        break
                for idx, time in enumerate(time_vec):
                    if time >= stop_time:
                        stop_idx = idx
                        break

            # cut signal and time vector to the right length               
            channel_1_cut = channel_1[start_idx:stop_idx]
            self.voice_signal = channel_1_cut
            time_vec_cut = time_vec[start_idx:stop_idx]

            # plot the signal on the first canvas
            self.canvas_voice.axes.plot(time_vec_cut, self.voice_signal)
            self.canvas_voice.axes.set_xlabel("Zeit in Sekunden")
            self.canvas_voice.axes.set_ylabel("Amplitude")
            self.canvas_voice.axes.set_title("Sprachsignal")
            self.canvas_voice.axes.grid(True)
            self.canvas_voice.draw()

            # create a spectrogram and plot it on the second canvas
            periodogram, freqs, bins, im = self.canvas_spectrogram.axes.specgram(channel_1, 1024, self.fs)
            self.canvas_spectrogram.axes.set_xlabel("Zeit in Sekunden")
            self.canvas_spectrogram.axes.set_ylabel("Frequenz in Hertz")
            self.canvas_spectrogram.axes.set_title("Spektrogramm")
            self.canvas_spectrogram.axes.set_ylim(0, 500)
            self.canvas_spectrogram.axes.grid(True)
            self.canvas_spectrogram.show()
            self.canvas_spectrogram.draw()

            self.button_analyse.setEnabled(True)
            self.button_save.setEnabled(False)

    def reset_plots(self):
        """
        Resets the plot by plotting the complete signal
        and deleting all information displayed. A new range
        can be chosen to be analysed.

        Called upon in the constructor of class "Acoustics_widget".
        """
        self.canvas_voice.axes.cla()
        self.canvas_spectrogram.axes.cla()
        self.textfield_time_start.setText("")
        self.textfield_time_end.setText("")
        self.label_fund_freq.setText("Grundfrequenz F0:")
        self.label_jitter.setText("Jitter:")
        self.label_shimmer.setText("Shimmer:")
        self.show_plots()

    def analyse(self):
        """
        Gets the current speech signal in a chosen range. Calculates 
        all acoustic parameters such as fundamental frequency, 
        jitter and shimmer and displays them.
        Analysis and code used from: https://osf.io/umrjq/ 
        last called: 30.05.22

        Called upon in constructor of class "Acoustics_widget".
        """
        self.show_plots()
        
        sound_obj = parselmouth.Sound(self.voice_signal)
        # max/min frequency chosen based on mean male and 
        # female fundamental frequency (TMA Skript)
        f0_min = 50
        f0_max = 300

        # calculate fundamental frequency
        pitch_obj = parselmouth.praat.call(sound_obj, "To Pitch", 0.0, f0_min, f0_max)
        fund_freq = parselmouth.praat.call(pitch_obj, "Get mean", 0, 0, "Hertz")
        fund_freq = round(fund_freq, 1)
        self.label_fund_freq.setText(f"Grundfrequenz F0: {fund_freq} Hz")

        # calculate jitter and shimmer
        point_process = parselmouth.praat.call(sound_obj, "To PointProcess (periodic, cc)", f0_min, f0_max)
        local_jitter = parselmouth.praat.call(point_process, "Get jitter (local)", 0, 0, 0.0001, 0.02, 1.3)
        local_jitter = round(local_jitter, 3)
        self.label_jitter.setText(f"Jitter: {local_jitter} %")
        local_shimmer =  parselmouth.praat.call([sound_obj, point_process], "Get shimmer (local)", 0, 0, 0.0001, 0.02, 1.3, 1.6)
        local_shimmer = round(local_shimmer, 3)
        self.label_shimmer.setText(f"Shimmer: {local_shimmer} %")

        self.evaluate_acoustics(local_jitter, local_shimmer)

        self.label_saved.setText("")
        self.button_save.setEnabled(True)

    def evaluate_acoustics(self, jitter, shimmer):
        """
        Evaluates whether the calculated Jitter and Shimmer values are within the norm.
        Reference values for assessment from: B. Schneider-Stickler 
        and W. Biegenzahn: "Stimmdiagnostik. Ein Leitfaden für die Praxis",
        Wien: Springer Verlag (2013)
        """
        
        if jitter >= 0.5 and jitter <= 1:
            self.label_jitter.setStyleSheet("background: green")
        else:
            self.label_jitter.setStyleSheet("background: red")

        if shimmer >= 0.5 and shimmer <= 4:
            self.label_shimmer.setStyleSheet("background: green")
        else:
            self.label_shimmer.setStyleSheet("background: red")

    def save(self):
        """
        Saves the results and comments for "Akustik", if 
        button is clicked. Emits a signal with the results to 
        "Data_handler" for further savings.

        Called upon in constructor of class "Acoustics_widget".
        """
        result_string = ""

        file_string = f"Analysierte Datei: {self.used_file_name}\n"

        start_time = self.textfield_time_start.text()
        stop_time = self.textfield_time_end.text()
        time_string = f"Ausgewählter Analysebereich: {start_time}s bis {stop_time}s"

        freq_string = self.label_fund_freq.text()
        jitter_string = self.label_jitter.text()
        shimmer_string = self.label_shimmer.text()

        comment = self.textfield_comment.text()
        comment_string = f"Weitere Kommentare: " + comment + "\n"

        result_string = "Akustik:\n"+file_string+time_string+"\n"+freq_string+"\n"+jitter_string+"\n"+shimmer_string+"\n"+comment_string
        self.signal_saved.emit(result_string)
        self.label_saved.setText("Aktuelle Analyse gespeichert.")

class Plot_canvas(Figure_canvas):
    """
    Constructor of class "Plot_canvas".

    Creates a plot canvas within the widget. Code used from:
    https://www.pythonguis.com/tutorials/pyside-plotting-matplotlib/
    last called: 01.06.22

    Called upon in constructor of class "EGG_widget".
    """
    def __init__(self, parent = None):
        figure = Figure()
        self.axes = figure.add_subplot(111)
        super(Plot_canvas, self).__init__(figure)