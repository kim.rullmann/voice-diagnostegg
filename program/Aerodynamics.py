"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
import program.Data_handler as Data_handler

class Aerodynamics_widget(QtWidgets.QWidget):
    signal_saved = QtCore.Signal(str)

    def __init__(self):
        """
        Constructor of class "Aerodynamics_widget".

        Sets a scene, creates all buttons and textfields.

        Called upon in constructor of class "Tab_widget" in "Main.py".
        """
        QtWidgets.QWidget.__init__(self)

        self.result_string = "Aerodynamische Messungen:\nNicht durchgeführt"

        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        self.label_VC = QtWidgets.QLabel("Vitalkapazität in ml: ")
        self.textfield_VC = QtWidgets.QLineEdit()
        self.label_MPT = QtWidgets.QLabel("max. Tonhaltedauer in s: ")
        self.textfield_MPT = QtWidgets.QLineEdit()

        self.button_evaluate = QtWidgets.QPushButton("Auswertung")
        self.button_evaluate.clicked.connect(self.evaluate_aerodynamics)

        self.label_VC_assessment = QtWidgets.QLabel("")
        self.label_MPT_assessment = QtWidgets.QLabel("")
        self.label_PQ = QtWidgets.QLabel("Phonationsquotient in ml/s: ")
        self.label_PQ_result = QtWidgets.QLabel("")
        self.label_PQ_assessment = QtWidgets.QLabel("")

        self.label_comment = QtWidgets.QLabel("Weitere Kommentare:")
        self.textfield_comment = QtWidgets.QLineEdit("")

        # create button to save result
        self.save_button = QtWidgets.QPushButton("Speichern")
        self.save_button.setEnabled(False)
        self.save_button.clicked.connect(self.save)
        self.label_saved = QtWidgets.QLabel("")
        label_placeholder = QtWidgets.QLabel()

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()

        self.layout.addWidget(label_placeholder, 0, 1, -1, -1)
        self.layout.addWidget(self.label_VC, 1, 1)
        self.layout.addWidget(self.textfield_VC, 1, 2, 1, 2)
        self.layout.addWidget(self.label_MPT, 2, 1)
        self.layout.addWidget(self.textfield_MPT, 2, 2, 1, 2)
        self.layout.addWidget(self.button_evaluate, 3, 1)
        self.layout.addWidget(self.label_VC_assessment, 4, 2)
        self.layout.addWidget(self.label_MPT_assessment, 5, 2)
        self.layout.addWidget(self.label_PQ, 6, 1)
        self.layout.addWidget(self.label_PQ_result, 6, 2)
        self.layout.addWidget(self.label_PQ_assessment, 6, 3)
        self.layout.addWidget(self.label_comment, 7, 1)
        self.layout.addWidget(self.textfield_comment, 7, 2, 1, 2)
        self.layout.addWidget(self.save_button, 8, 1)
        self.layout.addWidget(self.label_saved, 8, 2)
        self.layout.addWidget(label_placeholder, 9, 1, -1, -1)

        self.setLayout(self.layout)

    def calculate_PQ(self, vc, mpt):
        """
        Calculates the phonation quotient with given vital 
        capacity and maximum phonation time from textfields

        Parameters:
        -----------
        vc
                (float) value of vital capacity
        mpt
                (float) value of maximum phonation time
        
        Return:
        -------
        pq
                (float) value of phonation qoutient

        Called upon in function "evaluate_aerodynamics".
        """
        pq = vc/mpt
        pq = round(pq, 2)
        self.label_PQ_result.setText(str(pq))

        return pq

    def evaluate_aerodynamics(self):
        """
        Gets the values for vc and mpt from textfields. Calls upon
        the function "calculate_PQ", "get_vc_assessment" and
        "get_mpt_assessment" to get assessment for the values. Evaluates
        the result for PQ and displays assessment and result.

        Called upon in constructor of class "Aerodynamics_widget".
        """
        completed = self.check_completion()

        if completed == True:
            # get information from textfields
            vc = self.textfield_VC.text()
            mpt = self.textfield_MPT.text()

            if "," in vc:
                vc = vc.replace(",", ".")
            if "," in mpt:
                mpt = mpt.replace(",", ".")

            vc = float(vc)
            mpt = float(mpt)
            
            # calculate PQ and get assessment for vc, mpt
            # display results in widget
            pq = self.calculate_PQ(vc, mpt)
            vc_assessment = self.get_vc_assessment(vc)
            self.label_VC_assessment.setText("Vitalkapazität: " + vc_assessment)
            mpt_assessment = self.get_mpt_assessment(mpt)
            self.label_MPT_assessment.setText("Max. Tonhaltedauer: " + mpt_assessment)
            
            pq_assessment = ""
            if pq >= 120 and pq <= 190:
                pq_assessment = "Keine Einschränkung"
                self.label_PQ_assessment.setStyleSheet("background: green")
                self.label_PQ_result.setStyleSheet("background: green")
            elif pq > 190:
                pq_assessment = "Pathologisch"
                self.label_PQ_assessment.setStyleSheet("background: red")
                self.label_PQ_result.setStyleSheet("background: red")
            else:
                pq_assessment = "Kein Vergleich mit Referenzwerten möglich"
                self.label_PQ_assessment.setStyleSheet("background: yellow")
                self.label_PQ_result.setStyleSheet("background: yellow")
            self.label_PQ_assessment.setText(pq_assessment)

            self.save_button.setEnabled(True)
                      
    def check_completion(self):
        """
        Checks whether all textfields are filled in correctly.
        Sends messagebox if information is missing.

        Return:
        -------
        completed
                boolean which states if it is completed

        Called upon in function "evaluate_aerodynamics".
        """
        missing_info = []
        completed = True

        if self.textfield_VC.text() == "":
            missing_info.append("Vitalkapazität")
        if self. textfield_MPT.text() == "":
            missing_info.append("Maximale Tonhaltedauer")
        
        if missing_info != []:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setIcon(QtWidgets.QMessageBox.Critical)
            msg_box.setWindowTitle("Unvollständige Angaben")
            msg_box.setText("Bitte füllen Sie die Felder vollständig aus!")
            msg_box.setInformativeText(f"Folgende Angaben fehlen: {missing_info}")
            msg_box.exec_()
            completed = False
        
        return completed

    def save(self):
        """
        Saves the results and comments for "Aerodynamik", if 
        button is clicked. Emits a signal with the results to 
        "Data_handler" for further savings.

        Called upon in constructor of class "Aerodynamics_widget".
        """
        result_string = ""
        vc_assessment = self.label_VC_assessment.text()
        vc = self.textfield_VC.text()
        vc_string = vc_assessment + f" ({vc} ml)\n"

        mpt_assessment = self.label_MPT_assessment.text()
        mpt = self.textfield_MPT.text()
        mpt_string = mpt_assessment + f" ({mpt} s)\n"

        pq_assessment = "Phonationsquotient: " + self.label_PQ_assessment.text()
        pq = self.label_PQ_result.text()
        pq_string = pq_assessment + f" ({pq} ml/s)\n"

        comment = self.textfield_comment.text()
        comment_string = f"Weitere Kommentare: " + comment + "\n"

        result_string = "Aerodynamische Messungen:\n" + vc_string + mpt_string + pq_string + comment_string
        self.signal_saved.emit(result_string)
        self.label_saved.setText("Aktuelle Analyse gespeichert.")

    def get_vc_assessment(self, vc):
        """
        Gets the assessment for the vital capacity by getting patient
        information from "Data_handler".
        Reference values for assessment from: B. Schneider-Stickler 
        and W. Biegenzahn: "Stimmdiagnostik. Ein Leitfaden für die Praxis",
        Wien: Springer Verlag (2013)

        Parameter:
        ----------
        vc 
            (float) value for vital capacity

        Return:
        -------
        vc_assessment
            (string) vc assessment

        Called upon in function "evaluate_aerodynamics".
        """
        age = int(Data_handler.age)
        sex = Data_handler.sex
        height = int(Data_handler.height)
        vc_reference = 0

        if sex == "Männlich":
            if height < 170:
                if age <=29:
                    vc_reference = 4.37
                elif age >= 30 and age <= 39:
                    vc_reference = 4.07
                elif age >= 40 and age <= 49:
                    vc_reference = 3.76
                elif age >= 50 and age <= 59:
                    vc_reference = 3.45
                elif age >= 60 and age <= 69:
                    vc_reference = 3.14
                elif age >= 70 and age <= 79:
                    vc_reference = 2.84
                elif age >= 80:
                    vc_reference = 2.53
            elif height >= 170 and height < 180:
                if age <=29:
                    vc_reference = 5.24
                elif age >= 30 and age <= 39:
                    vc_reference = 4.88
                elif age >= 40 and age <= 49:
                    vc_reference = 4.51
                elif age >= 50 and age <= 59:
                    vc_reference = 4.14
                elif age >= 60 and age <= 69:
                    vc_reference = 3.77
                elif age >= 70 and age <= 79:
                    vc_reference = 3.40
                elif age >= 80:
                    vc_reference = 3.03
            elif height >= 180 and height < 190:
                if age <=29:
                    vc_reference = 6.23
                elif age >= 30 and age <= 39:
                    vc_reference = 5.79
                elif age >= 40 and age <= 49:
                    vc_reference = 5.35
                elif age >= 50 and age <= 59:
                    vc_reference = 4.91
                elif age >= 60 and age <= 69:
                    vc_reference = 4.48
                elif age >= 70 and age <= 79:
                    vc_reference = 4.04
                elif age >= 80:
                    vc_reference = 3.60
            elif height >= 190 and height < 200:
                if age <=29:
                    vc_reference = 7.32
                elif age >= 30 and age <= 39:
                    vc_reference = 6.81
                elif age >= 40 and age <= 49:
                    vc_reference = 6.29
                elif age >= 50 and age <= 59:
                    vc_reference = 5.78
                elif age >= 60 and age <= 69:
                    vc_reference = 5.26
                elif age >= 70 and age <= 79:
                    vc_reference = 4.75
                elif age >= 80:
                    vc_reference = 4.24
            elif height >= 200:
                if age <=29:
                    vc_reference = 8.54
                elif age >= 30 and age <= 39:
                    vc_reference = 7.94
                elif age >= 40 and age <= 49:
                    vc_reference = 7.34
                elif age >= 50 and age <= 59:
                    vc_reference = 6.74
                elif age >= 60 and age <= 69:
                    vc_reference = 6.14
                elif age >= 70 and age <= 79:
                    vc_reference = 5.54
                elif age >= 80:
                    vc_reference = 4.94
        elif sex == "Weiblich":
            if height < 170:
                if age <=29:
                    vc_reference = 3.97
                elif age >= 30 and age <= 39:
                    vc_reference = 3.70
                elif age >= 40 and age <= 49:
                    vc_reference = 3.42
                elif age >= 50 and age <= 59:
                    vc_reference = 3.14
                elif age >= 60 and age <= 69:
                    vc_reference = 2.86
                elif age >= 70 and age <= 79:
                    vc_reference = 2.58
                elif age >= 80:
                    vc_reference = 2.30
            elif height >= 170 and height < 180:
                if age <=29:
                    vc_reference = 4.77
                elif age >= 30 and age <= 39:
                    vc_reference = 4.43
                elif age >= 40 and age <= 49:
                    vc_reference = 4.10
                elif age >= 50 and age <= 59:
                    vc_reference = 3.76
                elif age >= 60 and age <= 69:
                    vc_reference = 3.43
                elif age >= 70 and age <= 79:
                    vc_reference = 3.09
                elif age >= 80:
                    vc_reference = 2.76
            elif height >= 180 and height < 190:
                if age <=29:
                    vc_reference = 5.66
                elif age >= 30 and age <= 39:
                    vc_reference = 5.26
                elif age >= 40 and age <= 49:
                    vc_reference = 4.86
                elif age >= 50 and age <= 59:
                    vc_reference = 4.47
                elif age >= 60 and age <= 69:
                    vc_reference = 4.07
                elif age >= 70 and age <= 79:
                    vc_reference = 3.67
                elif age >= 80:
                    vc_reference = 3.27
            elif height >= 190 and height < 200:
                if age <=29:
                    vc_reference = 6.66
                elif age >= 30 and age <= 39:
                    vc_reference = 6.19
                elif age >= 40 and age <= 49:
                    vc_reference = 5.72
                elif age >= 50 and age <= 59:
                    vc_reference = 5.25
                elif age >= 60 and age <= 69:
                    vc_reference = 4.79
                elif age >= 70 and age <= 79:
                    vc_reference = 4.32
                elif age >= 80:
                    vc_reference = 3.58
            elif height >= 200:
                if age <=29:
                    vc_reference = 7.76
                elif age >= 30 and age <= 39:
                    vc_reference = 7.22
                elif age >= 40 and age <= 49:
                    vc_reference = 6.67
                elif age >= 50 and age <= 59:
                    vc_reference = 6.13
                elif age >= 60 and age <= 69:
                    vc_reference = 5.58
                elif age >= 70 and age <= 79:
                    vc_reference = 5.04
                elif age >= 80:
                    vc_reference = 4.49

        vc_percentage = (vc / vc_reference) * 100
        vc_assessment = ""

        if vc_percentage > 90:
            vc_assessment = "Keine Einschränkung"
            self.label_VC_assessment.setStyleSheet("background: green")
        if vc_percentage <= 90 and vc_percentage > 70:
            vc_assessment = "Leichte Einschränkung"
            self.label_VC_assessment.setStyleSheet("background: yellow")
        if vc_percentage <= 70 and vc_percentage > 50:
            vc_assessment = "Mittelgradige Einschränkung"
            self.label_VC_assessment.setStyleSheet("background: orange")
        if vc_percentage <= 50:
            vc_assessment = "Schwere Einschränkung"
            self.label_VC_assessment.setStyleSheet("background: red")

        return vc_assessment

    def get_mpt_assessment(self, mpt):
        """
        Gets the assessment for the maximum phonation time.
        Reference values for assessment from: B. Schneider-Stickler 
        and W. Biegenzahn: "Stimmdiagnostik. Ein Leitfaden für die Praxis",
        Wien: Springer Verlag (2013)

        Parameter:
        ----------
        mpt 
                (float) value for maximum phonation time
        
        Return:
        -------
        mpt_assessment
                (string) mpt assessment
                
        Called upon in function "evaluate_aerodynamics".
        """
        mpt_assessment = ""
        if mpt > 15:
            mpt_assessment = "Keine Einschränkung"
            self.label_MPT_assessment.setStyleSheet("background: green")
        elif mpt <= 15 and mpt >= 11: 
            mpt_assessment = "Geringgradig pathologisch"
            self.label_MPT_assessment.setStyleSheet("background: yellow")
        elif mpt >= 10 and mpt >= 7:
            mpt_assessment = "Mittelgradig pathologisch"
            self.label_MPT_assessment.setStyleSheet("background: orange")
        elif mpt < 7:
            mpt_assessment = "Hochgradig pathologisch"
            self.label_MPT_assessment.setStyleSheet("background: red")

        return mpt_assessment
    