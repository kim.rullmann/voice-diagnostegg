"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui

class Perception_widget(QtWidgets.QWidget):
    signal_saved = QtCore.Signal(str)

    def __init__(self):
        """
        Constructor of class "Perception_widget".

        Creates a scene and radiobuttons to fill in the "RBH-Skala".
        Save button to save results for further saving in patient file.

        Called upon in constructor of class "Tab_widget" in "Main.py".
        """
        QtWidgets.QWidget.__init__(self)

        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        self.label_scale_0 = QtWidgets.QLabel("Nicht Vorhanden")
        self.label_scale_0.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_1 = QtWidgets.QLabel("Geringgradig Vorhanden")
        self.label_scale_1.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_2 = QtWidgets.QLabel("Mittelgradig Vorhanden")
        self.label_scale_2.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_3 = QtWidgets.QLabel("Hochgradig Vorhanden")
        self.label_scale_3.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))

        self.label_roughness = QtWidgets.QLabel("Rauigkeit: ")
        self.radiobutton_roughness_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_roughness_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_roughness_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_roughness_3 = QtWidgets.QRadioButton("3")

        self.label_breathiness = QtWidgets.QLabel("Behauchtheit: ")
        self.radiobutton_breathiness_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_breathiness_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_breathiness_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_breathiness_3 = QtWidgets.QRadioButton("3")

        self.label_hoarseness = QtWidgets.QLabel("Heiserkeit: ")
        self.radiobutton_hoarseness_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_hoarseness_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_hoarseness_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_hoarseness_3 = QtWidgets.QRadioButton("3")

        self.label_comment = QtWidgets.QLabel("Weitere Kommentare:")
        self.textfield_comment = QtWidgets.QLineEdit("")

        # create button to save result
        self.save_button = QtWidgets.QPushButton("Speichern")
        self.save_button.clicked.connect(self.save)
        self.label_saved = QtWidgets.QLabel("")
        self.play_audio_button = QtWidgets.QPushButton("")
        self.play_audio_button.setIcon(QtGui.QIcon("program\\icons\\speaker.jpeg"))
        label_placeholder = QtWidgets.QLabel()

        # add radio buttons into buttonGroups to make them exclusive
        self.group_roughness = QtWidgets.QButtonGroup(self)
        self.group_roughness.addButton(self.radiobutton_roughness_0)
        self.group_roughness.addButton(self.radiobutton_roughness_1)
        self.group_roughness.addButton(self.radiobutton_roughness_2)
        self.group_roughness.addButton(self.radiobutton_roughness_3)

        self.group_breathiness = QtWidgets.QButtonGroup(self)
        self.group_breathiness.addButton(self.radiobutton_breathiness_0)
        self.group_breathiness.addButton(self.radiobutton_breathiness_1)
        self.group_breathiness.addButton(self.radiobutton_breathiness_2)
        self.group_breathiness.addButton(self.radiobutton_breathiness_3)

        self.group_hoarseness = QtWidgets.QButtonGroup(self)
        self.group_hoarseness.addButton(self.radiobutton_hoarseness_0)
        self.group_hoarseness.addButton(self.radiobutton_hoarseness_1)
        self.group_hoarseness.addButton(self.radiobutton_hoarseness_2)
        self.group_hoarseness.addButton(self.radiobutton_hoarseness_3)

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()

        self.layout.addWidget(label_placeholder, 0, 1, -1, -1)
        self.layout.addWidget(self.label_scale_0, 1, 2, 1, 1)
        self.layout.addWidget(self.label_scale_1, 1, 3, 1, 1)
        self.layout.addWidget(self.label_scale_2, 1, 4, 1, 1)
        self.layout.addWidget(self.label_scale_3, 1, 5, 1, 1)

        self.layout.addWidget(self.label_roughness, 2, 1)
        self.layout.addWidget(self.radiobutton_roughness_0, 2, 2)
        self.layout.addWidget(self.radiobutton_roughness_1, 2, 3)
        self.layout.addWidget(self.radiobutton_roughness_2, 2, 4)
        self.layout.addWidget(self.radiobutton_roughness_3, 2, 5)

        self.layout.addWidget(self.label_breathiness, 3, 1)
        self.layout.addWidget(self.radiobutton_breathiness_0, 3, 2)
        self.layout.addWidget(self.radiobutton_breathiness_1, 3, 3)
        self.layout.addWidget(self.radiobutton_breathiness_2, 3, 4)
        self.layout.addWidget(self.radiobutton_breathiness_3, 3, 5)

        self.layout.addWidget(self.label_hoarseness, 4, 1)
        self.layout.addWidget(self.radiobutton_hoarseness_0, 4, 2)
        self.layout.addWidget(self.radiobutton_hoarseness_1, 4, 3)
        self.layout.addWidget(self.radiobutton_hoarseness_2, 4, 4)
        self.layout.addWidget(self.radiobutton_hoarseness_3, 4, 5)

        self.layout.addWidget(self.label_comment, 5, 1)
        self.layout.addWidget(self.textfield_comment, 5, 2, 1, 4)

        self.layout.addWidget(self.save_button, 6, 1)
        self.layout.addWidget(self.label_saved, 6, 2, 1, 3)
        self.layout.addWidget(self.play_audio_button, 6, 5)
        self.layout.addWidget(label_placeholder, 7, 1, -1, -1)      
    
        self.setLayout(self.layout)

    def save(self):
        """
        Saves the result and comments for "RBH-Skala", if button is clicked
        and everything is filled in. Emits a signal with the results to 
        "Data_handler" for further savings.

        Called upon in constructor of class "Perception_widget".
        """
        completed = self.check_completion()

        if completed == True:
            button_roughness = self.group_roughness.checkedButton()
            roughness = int(button_roughness.text())
            button_breathiness = self.group_breathiness.checkedButton()
            breathiness = int(button_breathiness.text())
            button_hoarseness = self.group_hoarseness.checkedButton()
            hoarseness = int(button_hoarseness.text())

            string_roughness = f"Rauigkeit: {self.get_rating_as_string(roughness)}\n"
            string_breathiness = f"Behauchtheit: {self.get_rating_as_string(breathiness)}\n"
            string_hoarseness = f"Heiserkeit: {self.get_rating_as_string(hoarseness)}\n"
            
            comment = self.textfield_comment.text()
            string_comment = f"Weitere Kommentare: " + comment + "\n"

            result_string = "RBH-Skala: \n"+string_roughness+string_breathiness+string_hoarseness+string_comment
            self.signal_saved.emit(result_string)
            self.label_saved.setText("Gespeichert.")

    def check_completion(self):
        """
        Checks whether a radiobutton in each buttongroup is checked.
        Sends messagebox if information is missing.

        Return:
        -------
        completed
                boolean which states if it is completed

        Called upon in function "save".
        """
        missing_rating = []
        completed = True

        if self.group_roughness.checkedButton() == None:
            missing_rating.append("Rauigkeit")
        if self.group_breathiness.checkedButton() == None:
            missing_rating.append("Behauchtheit")
        if self.group_hoarseness.checkedButton() == None:
            missing_rating.append("Heiserkeit")        

        if missing_rating != []:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setIcon(QtWidgets.QMessageBox.Critical)
            msg_box.setWindowTitle("Unvollständige Bewertung")
            msg_box.setText("Bitte füllen Sie den Fragebogen vollständig aus!")
            msg_box.setInformativeText(f"Folgende Angaben fehlen: {missing_rating}")
            msg_box.exec_()
            completed = False

        return completed

    def get_rating_as_string(self, value):
        """
        Gets rating information from radiobuttons and converts them as a 
        string to save in patient file.

        Parameter:
        ----------
        value
                (int) number from checked radiobutton
        
        Called upon in function "save".
        """
        rating = ""

        if value == 0:
            rating = "Nicht vorhanden"
        if value == 1:
            rating = "Geringgradig pathologisch"
        if value == 2:
            rating = "Mittelgradig pathologisch"
        if value == 3:
            rating = "Hochgradig pathologisch"

        return rating
