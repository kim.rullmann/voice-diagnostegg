"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
from datetime import date
import program.Data_handler as Data_handler

class Home_widget(QtWidgets.QWidget):
    signal_saved = QtCore.Signal(str)
    signal_anamnesis = QtCore.Signal(str)
    patient_dir = "\\patient info"

    def __init__(self):
        """
        Constructor of class "Home_widget".

        Creates a scene, textfields and buttons to either enter new 
        patients info or load patient info from an existing file.
        Write down an anamnesis and save new patient info.

        Called upon in constructor of class "Tab_widget" in "Main.py".
        """
        QtWidgets.QWidget.__init__(self) 

        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        self.today = date.today()
        self.today = self.today.strftime("%d.%m.%Y")
        self.label_date = QtWidgets.QLabel(self.today)
        self.label_date.setFont(QtGui.QFont("AnyStyle", 12, QtGui.QFont.Bold))
        self.label_info = QtWidgets.QLabel("Willkommen bei Voice DiagnostEGG!\n\nUm die Stimmdiagnostik zu beginnen, müssen Sie zunächst einen Patienten laden oder neu anlegen.\nIm Anschluss können Sie über den Menüreiter 'Datei' eine Datei laden oder eine neue aufnehmen.\nNun können über die verschiedenen Tabs die Messungen durchgeführt und gespeichert werden.")
        self.label_info.setFont(QtGui.QFont("AnyStyle", 10))

        self.button_load_patient_info = QtWidgets.QPushButton("Vorhandenen Patienten auswählen")
        self.button_load_patient_info.clicked.connect(self.read_patient_info_from_file)

        self.label_name = QtWidgets.QLabel("Name:")
        self.textfield_name = QtWidgets.QLineEdit("Max Mustermann")
        self.label_birthday = QtWidgets.QLabel("Geburtstag:")
        self.textfield_birthday = QtWidgets.QLineEdit("tt.mm.jjjj")
        self.textfield_birthday.editingFinished.connect(self.calculate_age)
        self.label_age = QtWidgets.QLabel("")
        self.label_sex = QtWidgets.QLabel("Geschlecht:")
        self.radiobutton_male = QtWidgets.QRadioButton("Männlich")
        self.radiobutton_female = QtWidgets.QRadioButton("Weiblich")
        self.group_sex = QtWidgets.QButtonGroup(self)
        self.group_sex.addButton(self.radiobutton_male)
        self.group_sex.addButton(self.radiobutton_female)
        self.label_height = QtWidgets.QLabel("Körpergröße:")
        self.textfield_height = QtWidgets.QLineEdit("")
        self.label_cm = QtWidgets.QLabel("cm")

        # connect anamnesis and save button to functions
        self.label_anamnesis = QtWidgets.QLabel("Anamnese: ")
        self.textfield_anamnesis = QtWidgets.QLineEdit("")
        self.textfield_anamnesis.editingFinished.connect(self.send_anamnesis)

        # create button to save patient info
        self.button_save_patient_info = QtWidgets.QPushButton("Patient speichern")
        self.button_save_patient_info.clicked.connect(self.send_patient_info)
        self.label_saved = QtWidgets.QLabel("")
        label_placeholder = QtWidgets.QLabel()

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()

        self.layout.addWidget(label_placeholder, 0, 1, -1, -1)
        self.layout.addWidget(self.label_date, 1, 1)
        self.layout.addWidget(self.label_info, 2, 1, 4, 4)
        self.layout.addWidget(self.button_load_patient_info, 6, 1)
        self.layout.addWidget(self.label_name, 7, 1)
        self.layout.addWidget(self.textfield_name, 7, 2, 1, 2)
        self.layout.addWidget(self.label_birthday, 8, 1)
        self.layout.addWidget(self.textfield_birthday, 8, 2, 1, 2)
        self.layout.addWidget(self.label_age, 8, 4)
        self.layout.addWidget(self.label_sex, 9, 1)
        self.layout.addWidget(self.radiobutton_male, 9, 2)
        self.layout.addWidget(self.radiobutton_female, 9, 3)
        self.layout.addWidget(self.label_height, 10, 1)
        self.layout.addWidget(self.textfield_height, 10, 2, 1, 2)
        self.layout.addWidget(self.label_cm, 10, 4)
        self.layout.addWidget(self.button_save_patient_info, 11, 1)
        self.layout.addWidget(self.label_saved, 11, 2)
        self.layout.addWidget(self.label_anamnesis, 12, 1)
        self.layout.addWidget(self.textfield_anamnesis, 12, 2, 1, 2)
        self.layout.addWidget(label_placeholder, 13, 1, -1, -1)

        self.setLayout(self.layout)

    def calculate_age(self):
        """
        Calculate age from patients birthday and todays date and
        sets label age next to birthday textfield.

        Called upon in constructor of class "Home_widget":
        """
        birth_date = self.textfield_birthday.text()
        birth_date_splitted = birth_date.split(".")
        birth_day = int(birth_date_splitted[0])
        birth_month = int(birth_date_splitted[1])
        birth_year = int(birth_date_splitted[2])
        
        current_date = date.today()
        current_year = int(current_date.year)
        current_month = int(current_date.month)
        current_day = int(current_date.day)

        self.age = current_year - birth_year

        if birth_month > current_month:
            self.age = self.age-1
        elif birth_month == current_month and birth_day > current_day:
            self.age = self.age-1

        self.label_age.setText(f"Alter: {self.age} Jahre")
    
    def read_patient_info_from_file(self):
        """
        Gets path and file from "Data_handler" and reads patient
        info from an existing file. Calls upon function "send_patient_info"
        display info.

        Called upon in constructor of class "Home_widget".
        """
        path = Data_handler.get_destination_path(self.patient_dir)
        file, filter = QtWidgets.QFileDialog.getOpenFileName(None, "Datei öffnen", path, "Text Files (*.txt)" )
        
        with open(file, 'r') as patient_file:
            data = [next(patient_file) for x in range(4)]
            patient_info = []
            for element in data:
                element_splitted = element.split(": ")
                element_splitted = element_splitted[1].split("\n")
                patient_info.append(element_splitted[0])
            self.textfield_name.setText(patient_info[0])
            self.textfield_birthday.setText(patient_info[1])
            self.calculate_age()
            if patient_info[2] == "Weiblich":
                self.radiobutton_female.setChecked(True)
            else: 
                self.radiobutton_male.setChecked(True)
            self.textfield_height.setText(patient_info[3].split(" ")[0])
            self.send_patient_info()

    def send_patient_info(self):
        """
        Sets patient info in textfields in Home menu when
        editing is finished. Emits signal that info is saved.

        Called upon in function "read_patient_info_from_file".
        """
        if self.check_completion() == True:

            date = self.today
            name = self.textfield_name.text()
            birthday = self.textfield_birthday.text()
            age = self.age
            sex = self.group_sex.checkedButton().text()
            height = self.textfield_height.text()

            info = f"{date},{name},{birthday},{age},{sex},{height}"
            self.signal_saved.emit(info)
            self.label_saved.setText("Gespeichert.")

    def check_completion(self):
        """
        Checks whether each textfield is filled in correctly.
        Sends messagebox if information is missing.

        Return:
        -------
        completed
                boolean which states if it is completed

        Called upon in function "send_patient_info".
        """
        missing_info = []
        completed = True

        if self.textfield_name.text() == "Max Mustermann":
            missing_info.append("Name")
        if self.textfield_birthday.text() == "tt.mm.jjjj":
            missing_info.append("Geburtstag")
        if self.group_sex.checkedButton() == None:
            missing_info.append("Geschlecht")
        if self.textfield_height.text() == "":
            missing_info.append("Körpergröße")

        if missing_info != []:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setIcon(QtWidgets.QMessageBox.Critical)
            msg_box.setWindowTitle("Unvollständige Angaben")
            msg_box.setText("Bitte füllen Sie die Felder vollständig aus!")
            msg_box.setInformativeText(f"Folgende Angaben fehlen: {missing_info}")
            msg_box.exec_()
            completed = False

        return completed

    def send_anamnesis(self):
        """
        Sends anamnesis which was filled in in anamnesis textfield.

        Called upon in constructor of class "Home_widget".
        """
        anamnesis = self.textfield_anamnesis.text()
        anamnesis_string = "Anamnese: "+anamnesis
        self.signal_anamnesis.emit(anamnesis_string)