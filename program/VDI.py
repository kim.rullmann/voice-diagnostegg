"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui

class VDI_widget(QtWidgets.QWidget):
    signal_saved = QtCore.Signal(str)

    def __init__(self):
        """
        Constructor of class "VDI_widget".

        Sets a scene, creates all questions, radio buttons, 
        buttongroups and textfields.

        Called upon in constructor of class "Tab_widget" in "Main.py".
        """
        QtWidgets.QWidget.__init__(self)

        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        # create labels for scale prompts
        self.label_scale_0 = QtWidgets.QLabel("Nie")
        self.label_scale_0.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_1 = QtWidgets.QLabel("Selten")
        self.label_scale_1.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_2 = QtWidgets.QLabel("Manchmal")
        self.label_scale_2.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_3 = QtWidgets.QLabel("Oft")
        self.label_scale_3.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))
        self.label_scale_4 = QtWidgets.QLabel("Immer")
        self.label_scale_4.setFont(QtGui.QFont("AnyStyle", 8, QtGui.QFont.Bold))

        # create labels with questions
        self.label_question_1 = QtWidgets.QLabel("1. Bevor ich spreche, weiß ich nicht, wie meine Stimme klingen wird.\n")
        self.label_question_2 = QtWidgets.QLabel("2. Abends ist meine Stimme schlechter.\n")
        self.label_question_3 = QtWidgets.QLabel("3. Ich habe das Gefühl, dass ich mich anstrengen muss, wenn ich meine Stimme benutze.\n")
        self.label_question_4 = QtWidgets.QLabel("4. Wegen meines Stimmproblems bin ich weniger kontaktfreudig.\n")
        self.label_question_5 = QtWidgets.QLabel("5. Ich meide größere Gruppen wegen meiner Stimme.\n")
        self.label_question_6 = QtWidgets.QLabel("6. Ich fühle mich bei Unterhaltungen wegen meiner Stimme ausgeschlossen.\n")
        self.label_question_7 = QtWidgets.QLabel("7. Anderen fällt es schwer, mich in einer lauten Umgebung zu verstehen.\n")
        self.label_question_8 = QtWidgets.QLabel("8. Meine Familie hat Schwierigkeiten, mich zu hören, wenn ich zuhause nach ihnen rufe.\n")
        self.label_question_9 = QtWidgets.QLabel("9. Man hört mich wegen meiner Stimme schlecht.\n")
        self.label_question_10 = QtWidgets.QLabel("10. Es ist mir peinlich, wenn man mich bittet, etwas zu widerholen.\n")
        self.label_question_11 = QtWidgets.QLabel("11. Ich ärgere mich, wenn man mich bittet, etwas zu wiederholen.\n")
        self.label_question_12 = QtWidgets.QLabel("12. Ich schäme mich wegen meines Stimmproblems.\n")

        # create a set of 5 radio buttons for each question
        self.radiobutton_q1_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q1_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q1_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q1_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q1_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q2_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q2_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q2_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q2_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q2_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q3_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q3_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q3_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q3_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q3_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q4_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q4_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q4_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q4_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q4_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q5_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q5_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q5_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q5_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q5_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q6_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q6_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q6_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q6_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q6_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q7_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q7_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q7_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q7_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q7_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q8_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q8_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q8_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q8_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q8_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q9_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q9_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q9_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q9_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q9_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q10_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q10_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q10_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q10_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q10_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q11_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q11_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q11_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q11_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q11_4 = QtWidgets.QRadioButton("4")

        self.radiobutton_q12_0 = QtWidgets.QRadioButton("0")
        self.radiobutton_q12_1 = QtWidgets.QRadioButton("1")
        self.radiobutton_q12_2 = QtWidgets.QRadioButton("2")
        self.radiobutton_q12_3 = QtWidgets.QRadioButton("3")
        self.radiobutton_q12_4 = QtWidgets.QRadioButton("4")

        # add radio buttons into buttonGroups to make them exclusive
        self.group_q1 = QtWidgets.QButtonGroup(self)
        self.group_q1.addButton(self.radiobutton_q1_0)
        self.group_q1.addButton(self.radiobutton_q1_1)
        self.group_q1.addButton(self.radiobutton_q1_2)
        self.group_q1.addButton(self.radiobutton_q1_3)
        self.group_q1.addButton(self.radiobutton_q1_4)

        self.group_q2 = QtWidgets.QButtonGroup(self)
        self.group_q2.addButton(self.radiobutton_q2_0)
        self.group_q2.addButton(self.radiobutton_q2_1)
        self.group_q2.addButton(self.radiobutton_q2_2)
        self.group_q2.addButton(self.radiobutton_q2_3)
        self.group_q2.addButton(self.radiobutton_q2_4)

        self.group_q3 = QtWidgets.QButtonGroup(self)
        self.group_q3.addButton(self.radiobutton_q3_0)
        self.group_q3.addButton(self.radiobutton_q3_1)
        self.group_q3.addButton(self.radiobutton_q3_2)
        self.group_q3.addButton(self.radiobutton_q3_3)
        self.group_q3.addButton(self.radiobutton_q3_4)

        self.group_q4 = QtWidgets.QButtonGroup(self)
        self.group_q4.addButton(self.radiobutton_q4_0)
        self.group_q4.addButton(self.radiobutton_q4_1)
        self.group_q4.addButton(self.radiobutton_q4_2)
        self.group_q4.addButton(self.radiobutton_q4_3)
        self.group_q4.addButton(self.radiobutton_q4_4)

        self.group_q5 = QtWidgets.QButtonGroup(self)
        self.group_q5.addButton(self.radiobutton_q5_0)
        self.group_q5.addButton(self.radiobutton_q5_1)
        self.group_q5.addButton(self.radiobutton_q5_2)
        self.group_q5.addButton(self.radiobutton_q5_3)
        self.group_q5.addButton(self.radiobutton_q5_4)

        self.group_q6 = QtWidgets.QButtonGroup(self)
        self.group_q6.addButton(self.radiobutton_q6_0)
        self.group_q6.addButton(self.radiobutton_q6_1)
        self.group_q6.addButton(self.radiobutton_q6_2)
        self.group_q6.addButton(self.radiobutton_q6_3)
        self.group_q6.addButton(self.radiobutton_q6_4)

        self.group_q7 = QtWidgets.QButtonGroup(self)
        self.group_q7.addButton(self.radiobutton_q7_0)
        self.group_q7.addButton(self.radiobutton_q7_1)
        self.group_q7.addButton(self.radiobutton_q7_2)
        self.group_q7.addButton(self.radiobutton_q7_3)
        self.group_q7.addButton(self.radiobutton_q7_4)

        self.group_q8 = QtWidgets.QButtonGroup(self)
        self.group_q8.addButton(self.radiobutton_q8_0)
        self.group_q8.addButton(self.radiobutton_q8_1)
        self.group_q8.addButton(self.radiobutton_q8_2)
        self.group_q8.addButton(self.radiobutton_q8_3)
        self.group_q8.addButton(self.radiobutton_q8_4)

        self.group_q9 = QtWidgets.QButtonGroup(self)
        self.group_q9.addButton(self.radiobutton_q9_0)
        self.group_q9.addButton(self.radiobutton_q9_1)
        self.group_q9.addButton(self.radiobutton_q9_2)
        self.group_q9.addButton(self.radiobutton_q9_3)
        self.group_q9.addButton(self.radiobutton_q9_4)

        self.group_q10 = QtWidgets.QButtonGroup(self)
        self.group_q10.addButton(self.radiobutton_q10_0)
        self.group_q10.addButton(self.radiobutton_q10_1)
        self.group_q10.addButton(self.radiobutton_q10_2)
        self.group_q10.addButton(self.radiobutton_q10_3)
        self.group_q10.addButton(self.radiobutton_q10_4)

        self.group_q11 = QtWidgets.QButtonGroup(self)
        self.group_q11.addButton(self.radiobutton_q11_0)
        self.group_q11.addButton(self.radiobutton_q11_1)
        self.group_q11.addButton(self.radiobutton_q11_2)
        self.group_q11.addButton(self.radiobutton_q11_3)
        self.group_q11.addButton(self.radiobutton_q11_4)

        self.group_q12 = QtWidgets.QButtonGroup(self)
        self.group_q12.addButton(self.radiobutton_q12_0)
        self.group_q12.addButton(self.radiobutton_q12_1)
        self.group_q12.addButton(self.radiobutton_q12_2)
        self.group_q12.addButton(self.radiobutton_q12_3)
        self.group_q12.addButton(self.radiobutton_q12_4)

        # create button and label for evaluation
        self.evaluation_button = QtWidgets.QPushButton("Auswertung")
        self.evaluation_button.clicked.connect(self.evaluate)
        self.evaluation_label = QtWidgets.QLabel("")

        self.label_comment = QtWidgets.QLabel("Weitere Kommentare:")
        self.textfield_comment = QtWidgets.QLineEdit("")

        # create button to save result
        self.save_button = QtWidgets.QPushButton("Speichern")
        self.save_button.setEnabled(False)
        self.save_button.clicked.connect(self.save)
        self.label_saved = QtWidgets.QLabel("")

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()

        self.layout.addWidget(self.label_scale_0, 1, 2)
        self.layout.addWidget(self.label_scale_1, 1, 3)
        self.layout.addWidget(self.label_scale_2, 1, 4)
        self.layout.addWidget(self.label_scale_3, 1, 5)
        self.layout.addWidget(self.label_scale_4, 1, 6)

        self.layout.addWidget(self.label_question_1, 2, 1)
        self.layout.addWidget(self.radiobutton_q1_0, 2, 2)
        self.layout.addWidget(self.radiobutton_q1_1, 2, 3)
        self.layout.addWidget(self.radiobutton_q1_2, 2, 4)
        self.layout.addWidget(self.radiobutton_q1_3, 2, 5)
        self.layout.addWidget(self.radiobutton_q1_4, 2, 6)

        self.layout.addWidget(self.label_question_2, 3, 1)
        self.layout.addWidget(self.radiobutton_q2_0, 3, 2)
        self.layout.addWidget(self.radiobutton_q2_1, 3, 3)
        self.layout.addWidget(self.radiobutton_q2_2, 3, 4)
        self.layout.addWidget(self.radiobutton_q2_3, 3, 5)
        self.layout.addWidget(self.radiobutton_q2_4, 3, 6)

        self.layout.addWidget(self.label_question_3, 4, 1)
        self.layout.addWidget(self.radiobutton_q3_0, 4, 2)
        self.layout.addWidget(self.radiobutton_q3_1, 4, 3)
        self.layout.addWidget(self.radiobutton_q3_2, 4, 4)
        self.layout.addWidget(self.radiobutton_q3_3, 4, 5)
        self.layout.addWidget(self.radiobutton_q3_4, 4, 6)

        self.layout.addWidget(self.label_question_4, 5, 1)
        self.layout.addWidget(self.radiobutton_q4_0, 5, 2)
        self.layout.addWidget(self.radiobutton_q4_1, 5, 3)
        self.layout.addWidget(self.radiobutton_q4_2, 5, 4)
        self.layout.addWidget(self.radiobutton_q4_3, 5, 5)
        self.layout.addWidget(self.radiobutton_q4_4, 5, 6)

        self.layout.addWidget(self.label_question_5, 6, 1)
        self.layout.addWidget(self.radiobutton_q5_0, 6, 2)
        self.layout.addWidget(self.radiobutton_q5_1, 6, 3)
        self.layout.addWidget(self.radiobutton_q5_2, 6, 4)
        self.layout.addWidget(self.radiobutton_q5_3, 6, 5)
        self.layout.addWidget(self.radiobutton_q5_4, 6, 6)

        self.layout.addWidget(self.label_question_6, 7, 1)
        self.layout.addWidget(self.radiobutton_q6_0, 7, 2)
        self.layout.addWidget(self.radiobutton_q6_1, 7, 3)
        self.layout.addWidget(self.radiobutton_q6_2, 7, 4)
        self.layout.addWidget(self.radiobutton_q6_3, 7, 5)
        self.layout.addWidget(self.radiobutton_q6_4, 7, 6)

        self.layout.addWidget(self.label_question_7, 8, 1)
        self.layout.addWidget(self.radiobutton_q7_0, 8, 2)
        self.layout.addWidget(self.radiobutton_q7_1, 8, 3)
        self.layout.addWidget(self.radiobutton_q7_2, 8, 4)
        self.layout.addWidget(self.radiobutton_q7_3, 8, 5)
        self.layout.addWidget(self.radiobutton_q7_4, 8, 6)

        self.layout.addWidget(self.label_question_8, 9, 1)
        self.layout.addWidget(self.radiobutton_q8_0, 9, 2)
        self.layout.addWidget(self.radiobutton_q8_1, 9, 3)
        self.layout.addWidget(self.radiobutton_q8_2, 9, 4)
        self.layout.addWidget(self.radiobutton_q8_3, 9, 5)
        self.layout.addWidget(self.radiobutton_q8_4, 9, 6)

        self.layout.addWidget(self.label_question_9, 10, 1)
        self.layout.addWidget(self.radiobutton_q9_0, 10, 2)
        self.layout.addWidget(self.radiobutton_q9_1, 10, 3)
        self.layout.addWidget(self.radiobutton_q9_2, 10, 4)
        self.layout.addWidget(self.radiobutton_q9_3, 10, 5)
        self.layout.addWidget(self.radiobutton_q9_4, 10, 6)

        self.layout.addWidget(self.label_question_10, 11, 1)
        self.layout.addWidget(self.radiobutton_q10_0, 11, 2)
        self.layout.addWidget(self.radiobutton_q10_1, 11, 3)
        self.layout.addWidget(self.radiobutton_q10_2, 11, 4)
        self.layout.addWidget(self.radiobutton_q10_3, 11, 5)
        self.layout.addWidget(self.radiobutton_q10_4, 11, 6)

        self.layout.addWidget(self.label_question_11, 12, 1)
        self.layout.addWidget(self.radiobutton_q11_0, 12, 2)
        self.layout.addWidget(self.radiobutton_q11_1, 12, 3)
        self.layout.addWidget(self.radiobutton_q11_2, 12, 4)
        self.layout.addWidget(self.radiobutton_q11_3, 12, 5)
        self.layout.addWidget(self.radiobutton_q11_4, 12, 6)

        self.layout.addWidget(self.label_question_12, 13, 1)
        self.layout.addWidget(self.radiobutton_q12_0, 13, 2)
        self.layout.addWidget(self.radiobutton_q12_1, 13, 3)
        self.layout.addWidget(self.radiobutton_q12_2, 13, 4)
        self.layout.addWidget(self.radiobutton_q12_3, 13, 5)
        self.layout.addWidget(self.radiobutton_q12_4, 13, 6)

        self.layout.addWidget(self.evaluation_button, 14, 1, 1, 1)
        self.layout.addWidget(self.evaluation_label, 14, 2, 4, 5)
        self.layout.addWidget(self.label_comment, 18, 1, 1, 1)
        self.layout.addWidget(self.textfield_comment, 18, 2, 1, 5) 
        self.layout.addWidget(self.save_button, 19, 2, 1, 2)
        self.layout.addWidget(self.label_saved, 19, 4, 1, 2)

        self.setLayout(self.layout)

    def evaluate(self):
        """
        Evaluate the checked radiobuttons by casting and 
        analysing them to display result on widget.

        Called upon in constructor of class "VDI_widget".
        """
        completed = self.check_completion()

        if completed == True:
            
            # cast button text to int values
            button_q1 = self.group_q1.checkedButton()
            value_q1 = int(button_q1.text())
            button_q2 = self.group_q2.checkedButton()
            value_q2 = int(button_q2.text())
            button_q3 = self.group_q3.checkedButton()
            value_q3 = int(button_q3.text())
            button_q4 = self.group_q4.checkedButton()
            value_q4 = int(button_q4.text())
            button_q5 = self.group_q5.checkedButton()
            value_q5 = int(button_q5.text())
            button_q6 = self.group_q6.checkedButton()
            value_q6 = int(button_q6.text())
            button_q7 = self.group_q7.checkedButton()
            value_q7 = int(button_q7.text())
            button_q8 = self.group_q8.checkedButton()
            value_q8 = int(button_q8.text())
            button_q9 = self.group_q9.checkedButton()
            value_q9 = int(button_q9.text())
            button_q10 = self.group_q10.checkedButton()
            value_q10 = int(button_q10.text())
            button_q11 = self.group_q11.checkedButton()
            value_q11 = int(button_q11.text())
            button_q12 = self.group_q12.checkedButton()
            value_q12 = int(button_q12.text())

            # calculate sum for each question block
            sum_functional = value_q5+value_q6+value_q7+value_q8+value_q9
            sum_physiological = value_q1+value_q2+value_q3
            sum_emotional = value_q4+value_q10+value_q11+value_q12
            sum_total = sum_functional + sum_physiological + sum_emotional
            
            result = ""
            if sum_total <= 7:
                result = "Keine Einschränkung"
                self.evaluation_label.setStyleSheet("background: green")
            elif sum_total > 7 and sum_total <= 14:
                result = "Leichte Einschränkung"
                self.evaluation_label.setStyleSheet("background: yellow")
            elif sum_total > 14 and sum_total <= 22:
                result = "Mittelgradige Einschränkung"
                self.evaluation_label.setStyleSheet("background: orange")
            elif sum_total > 22:
                result = "Hochgradige Einschränkung"
                self.evaluation_label.setStyleSheet("background: red")

            # display result
            subscale_results = f"\nFunktional: {sum_functional} Punkte\nPhysisch: {sum_physiological} Punkte\nEmotional: {sum_emotional} Punkte"
            self.complete_result = result+f" ({sum_total} Punkte)"+subscale_results
            self.evaluation_label.setText(self.complete_result)
            self.save_button.setEnabled(True)

    def save(self):
        """
        Saves the results and comments for "VDI", if 
        button is clicked. Emits a signal with the results to 
        "Data_handler" for further savings.

        Called upon in constructor of class "VDI_widget".
        """
        result_string = ""

        comment = self.textfield_comment.text()
        comment_string = f"Weitere Kommentare: " + comment + "\n"

        result_string = "SSI:\n"+self.complete_result+"\n"+comment_string+"\n"
        self.signal_saved.emit(result_string)
        self.label_saved.setText("Gespeichert.")

    def check_completion(self):
        """
        Checks whether all buttongroups are checked.
        Sends messagebox if information is missing.

        Return:
        -------
        completed
                boolean which states if it is completed

        Called upon in function "evaluate".
        """
        missing_q = []
        completed = True

        if self.group_q1.checkedButton() == None:
            missing_q.append(1)
        if self.group_q2.checkedButton() == None:
            missing_q.append(2)
        if self.group_q3.checkedButton() == None:
            missing_q.append(3)
        if self.group_q4.checkedButton() == None:
            missing_q.append(4)
        if self.group_q5.checkedButton() == None:
            missing_q.append(5)
        if self.group_q6.checkedButton() == None:
            missing_q.append(6)
        if self.group_q7.checkedButton() == None:
            missing_q.append(7)
        if self.group_q8.checkedButton() == None:
            missing_q.append(8)
        if self.group_q9.checkedButton() == None:
            missing_q.append(9)
        if self.group_q10.checkedButton() == None:
            missing_q.append(10)
        if self.group_q11.checkedButton() == None:
            missing_q.append(11)
        if self.group_q12.checkedButton() == None:
            missing_q.append(12)

        if missing_q != []:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setIcon(QtWidgets.QMessageBox.Critical)
            msg_box.setWindowTitle("Unvollständiger Fragebogen")
            msg_box.setText("Bitte füllen Sie den Fragebogen vollständig aus!")
            msg_box.setInformativeText(f"Folgende Fragen wurden nicht beantwortet: {missing_q}")
            msg_box.exec_()
            completed = False

        return completed   