"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Figure_canvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as Navigation_toolbar
from matplotlib.figure import Figure
import scipy.signal
import program.Data_handler as Data_handler

class EGG_widget(QtWidgets.QWidget):
    signal_saved = QtCore.Signal(str)
    used_file_name = ""
    Lx_waveform = []
    fs = 48000

    def __init__(self):
        """
        Constructor of class "EGG_widget".

        Sets a scene, creates an object of a plot canvas and 
        navigation toolbar and sets all buttons and textfields.

        Called upon in constructor of class "Tab_widget" in "Main.py".
        """
        QtWidgets.QWidget.__init__(self)

        self.scene = QtWidgets.QGraphicsScene()
        self.graphics_view = QtWidgets.QGraphicsView()
        self.graphics_view.setScene(self.scene)
        self.graphics_view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.graphics_view.scale(1,-1)

        self.canvas = Plot_canvas(self)
        self.toolbar = Navigation_toolbar(self.canvas, self)
        
        label_time_range = QtWidgets.QLabel("Zu analysierenden Zeitbereich auswählen: ")
        label_time_start = QtWidgets.QLabel("von (in sek): ")
        self.textfield_time_start = QtWidgets.QLineEdit("")
        label_time_end = QtWidgets.QLabel("bis (in sek): ")
        self.textfield_time_end = QtWidgets.QLineEdit("")

        self.button_analyse = QtWidgets.QPushButton("Analyse")
        self.button_analyse.clicked.connect(self.analyse)
        self.button_analyse.setEnabled(False)
        button_reset_plot = QtWidgets.QPushButton("Zurücksetzen")
        button_reset_plot.clicked.connect(self.reset_plot)

        self.label_fund_freq = QtWidgets.QLabel("Grundfrequenz F0: ")
        self.label_fund_period = QtWidgets.QLabel("Periodendauer T0: ")
        self.label_closed_phase = QtWidgets.QLabel("Closed Phase CP: ")
        self.label_closed_quotient = QtWidgets.QLabel("Closed Quotient CQ: ")
        self.label_open_phase = QtWidgets.QLabel("Open Phase OP: ")
        self.label_open_quotient = QtWidgets.QLabel("Open Quotient OQ: ")

        self.label_comment = QtWidgets.QLabel("Weitere Kommentare:")
        self.textfield_comment = QtWidgets.QLineEdit("")

        # create button to save result
        self.button_save = QtWidgets.QPushButton("Speichern")
        self.button_save.clicked.connect(self.save)
        self.button_save.setEnabled(False)
        self.label_saved = QtWidgets.QLabel("")

        # create layout and add widgets within
        self.layout = QtWidgets.QGridLayout()

        self.layout.addWidget(self.toolbar, 1, 3, 1, -1)
        self.layout.addWidget(self.canvas, 2, 1, 5, -1)
        self.layout.addWidget(label_time_range, 7, 1)
        self.layout.addWidget(label_time_start, 7, 2)
        self.layout.addWidget(self.textfield_time_start, 7, 3)
        self.layout.addWidget(label_time_end, 7, 4)
        self.layout.addWidget(self.textfield_time_end, 7, 5)
        self.layout.addWidget(self.button_analyse, 8, 1)
        self.layout.addWidget(button_reset_plot, 8, 5)
        self.layout.addWidget(self.label_fund_freq, 9, 2, 1, 2)
        self.layout.addWidget(self.label_fund_period, 9, 4, 1, 2)
        self.layout.addWidget(self.label_closed_phase, 10, 2, 1, 2)
        self.layout.addWidget(self.label_closed_quotient, 10, 4, 1, 2)
        self.layout.addWidget(self.label_open_phase, 11, 2, 1, 2)
        self.layout.addWidget(self.label_open_quotient, 11, 4, 1, 2)
        self.layout.addWidget(self.label_comment, 12, 1)
        self.layout.addWidget(self.textfield_comment, 12, 2, 1, 4)
        self.layout.addWidget(self.button_save, 13, 1)
        self.layout.addWidget(self.label_saved, 13, 2, 1, -1)
        self.setLayout(self.layout)

    def get_Lx(self):
        """
        Gets the Lx signal from channel 2 of the soundcard, normalises
        the signal and cuts it to the chosen range with the information
        from textfields. Plots the signal on the canvas.

        Called upon in function "analyse" and "reset_plot".
        """
        self.fs, signal, self.used_file_name = Data_handler.get_signal()
        if signal != []:
            # normalise signal
            channel_2 = signal[:,1]
            max_value = max(abs(channel_2))
            channel_2_normalised = channel_2/max_value

            # create time vector
            time_vec = np.linspace(0, len(channel_2_normalised)-1, len(channel_2_normalised))
            time_vec = time_vec/self.fs

            # get range to be analysed
            start_time = self.textfield_time_start.text()
            stop_time = self.textfield_time_end.text()
            if "," in start_time:
                start_time = start_time.replace(",", ".")
            if "," in stop_time:
                stop_time = stop_time.replace(",", ".")

            start_idx = 0
            stop_idx = len(channel_2_normalised)-1

            if start_time != "" and stop_time != "" and start_time < stop_time:             
                start_time = float(start_time)
                stop_time = float(stop_time)
                
                for idx, time in enumerate(time_vec):
                    if time >= start_time:
                        start_idx = idx
                        break
                for idx, time in enumerate(time_vec):
                    if time >= stop_time:
                        stop_idx = idx
                        break

            # cut Lx signal and time vector to the right length                   
            channel_2_normalised_cut = -channel_2_normalised[start_idx:stop_idx]
            self.Lx_waveform = channel_2_normalised_cut
            time_vec_cut = time_vec[start_idx:stop_idx]

            # plot it on the canvas
            self.canvas.axes.plot(time_vec_cut, self.Lx_waveform)
            self.canvas.axes.set_xlabel("Zeit in Sekunden")
            self.canvas.axes.set_ylabel("Amplitude (normalisiert)")
            self.canvas.axes.set_title("Lx Signal")
            self.canvas.axes.grid(True)
            self.canvas.draw()
            self.button_analyse.setEnabled(True)
            self.button_save.setEnabled(False)

    def reset_plot(self):
        """
        Resets the plot by plotting the complete Lx Signal
        and deleting all information displayed. A new range
        can be chosen to be analysed.

        Called upon in the constructor of class "EGG_widget".
        """
        self.canvas.axes.cla()
        self.textfield_time_start.setText("")
        self.textfield_time_end.setText("")
        self.label_fund_freq.setText("Grundfrequenz F0: ")
        self.label_fund_period.setText("Periodendauer T0: ")
        self.label_closed_phase.setText("Closed Phase CP: ")
        self.label_closed_quotient.setText("Closed Quotient CQ: ")
        self.label_open_phase.setText("Open Phase OP: ")
        self.label_open_quotient.setText("Open Quotient OQ: ")
        self.get_Lx()

    def analyse(self):
        """
        Gets the current signal in a chosen range. Calculates the
        first derivative and finds its minimum and maximum peaks.
        Calculates with this information parameters of the EGG such
        as fundamental period and frequency, open and closed phase and 
        open and closed quotient.
        
        Analysis is based upon: Howard (2009): 
        "Electroglottography/electrolaryngography", chapter 13. 

        Called upon in the constructor of class "EGG_widget".
        """
        self.get_Lx()  

        # create time vector, calculate first derivative and normalise it
        time_vec = np.linspace(0, len(self.Lx_waveform)-1, len(self.Lx_waveform))
        time_vec = time_vec/self.fs
        Lx_gradient = np.diff(self.Lx_waveform)/np.diff(time_vec)
        Lx_max = max(abs(Lx_gradient))
        Lx_gradient_normalised = Lx_gradient / Lx_max
        
        # find maxima in Lx gradient and calculate mean gap inbetween
        # which defines the fundamental period
        # women's fundamental frequency is around 250Hz (Skript TMA) 
        # -> max. distance between two peaks is around 1/300
        maxima_indices_samples = scipy.signal.find_peaks(Lx_gradient_normalised, height=0.7, distance=((1/300)*self.fs))
        maxima_indices_time = maxima_indices_samples[0]/self.fs
        delta_max_max = []
        first_value = maxima_indices_time[0]
        for maximum in maxima_indices_time[1:len(maxima_indices_time)-1]:
            second_value = maximum
            delta = second_value - first_value
            delta_max_max.append(delta)
            first_value = second_value
        fund_period = np.mean(delta_max_max)
        fund_period_ms = round(fund_period*10**(3),2)
        self.label_fund_period.setText(f"Periodendauer T0: {fund_period_ms} ms")
        fund_freq = 1/fund_period
        fund_freq = round(fund_freq,1)
        self.label_fund_freq.setText(f"Grundfrequenz f0: {fund_freq} Hz")

        # find minima in Lx gradient
        minima_indices_samples = scipy.signal.find_peaks(-Lx_gradient_normalised, height=0.05, distance=((1/300)*self.fs))
        minima_indices_time = minima_indices_samples[0]/self.fs

        # create list where each maximum is followed by next 
        # minimum
        max_min_list = [maxima_indices_time[0]]
        end_reached = False
        idx_min = 0
        idx_max = 0
        minimum = minima_indices_time[idx_min]
        maximum = maxima_indices_time[idx_max]
        while end_reached == False:

            while minimum < maximum and idx_min < len(minima_indices_time):
                minimum = minima_indices_time[idx_min]
                idx_min += 1
            max_min_list.extend([minimum])

            while maximum < minimum and idx_max < len(maxima_indices_time):
                maximum = maxima_indices_time[idx_max]
                idx_max += 1
            max_min_list.extend([maximum])

            if idx_max == len(maxima_indices_time) or idx_min == len(minima_indices_time):
                end_reached = True

        # go through list of maxima followed by minima and 
        # calculate differences
        # difference maximum to minimum --> closed phase
        # difference minimum to maximum --> open phase
        first_value = max_min_list[0]
        delta_max_min = []
        delta_min_max = []
        open_phase = False
        for value in max_min_list[1:len(max_min_list)-1]:
            second_value = value
            delta = second_value - first_value
            if open_phase == True:
                delta_min_max.append(delta)
                open_phase = False
            else:
                delta_max_min.append(delta)
                open_phase = True
            first_value = second_value

        # calculate mean of lists of closed and open phase
        # calculate open and closed quotient
        # set text to display analysis
        closed_phase = np.mean(delta_max_min)
        closed_phase_ms = round(closed_phase*10**(3), 2)
        self.label_closed_phase.setText(f"Closed Phase CP: {closed_phase_ms} ms")
        closed_quotient = closed_phase/fund_period * 100
        closed_quotient = round(closed_quotient, 2)
        self.label_closed_quotient.setText(f"Closed Quotient CQ: {closed_quotient} %")
        
        open_phase = np.mean(delta_min_max)
        open_phase_ms = round(open_phase*10**(3), 2)
        self.label_open_phase.setText(f"Open Phase OP: {open_phase_ms} ms")
        open_quotient = open_phase/fund_period * 100
        open_quotient = round(open_quotient, 2)
        self.label_open_quotient.setText(f"Open Quotient OQ: {open_quotient} %")
                  
        self.button_save.setEnabled(True)

    def save(self):
        """
        Saves the results and comments for "Elektroglottographie", if 
        button is clicked. Emits a signal with the results to 
        "Data_handler" for further savings.

        Called upon in constructor of class "EGG_widget".
        """
        result_string = ""

        file_string = f"Analysierte Datei: {self.used_file_name}\n"

        start_time = self.textfield_time_start.text()
        stop_time = self.textfield_time_end.text()
        time_string = f"Ausgewählter Analysebereich: {start_time}s bis {stop_time}s"

        freq_string = self.label_fund_freq.text()
        period_string = self.label_fund_period.text()
        cp_string = self.label_closed_phase.text()
        cq_string = self.label_closed_quotient.text()
        op_string = self.label_open_phase.text()
        oq_string = self.label_open_quotient.text()

        comment = self.textfield_comment.text()
        comment_string = f"Weitere Kommentare: " + comment + "\n"

        result_string = "EGG:\n" + file_string + time_string + "\n" + freq_string + "\n" + period_string+"\n"+cp_string+"\n"+cq_string+"\n"+op_string+"\n"+oq_string+"\n"+comment_string
        self.signal_saved.emit(result_string)
        self.label_saved.setText("Aktuelle Analyse gespeichert.")

class Plot_canvas(Figure_canvas):
    def __init__(self, parent = None):
        """
        Constructor of class "Plot_canvas".

        Creates a plot canvas within the widget. Code used from:
        https://www.pythonguis.com/tutorials/pyside-plotting-matplotlib/
        last called: 01.06.22

        Called upon in constructor of class "EGG_widget".
        """
        figure = Figure()
        self.axes = figure.add_subplot(111)
        super(Plot_canvas, self).__init__(figure)