"""
Copyright (c) 2022, Kim Rullmann: kim.rullmann@student.jade-hs.de
                    Annabell Urban: annabell.urban@student.jade-hs.de
This code is published under the terms of the GPL license.

"""

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
from scipy.io import wavfile
import soundcard
import threading
from datetime import datetime
import numpy as np
import os.path 

# defining various global variables
signal = []
fs = 48000

used_file_name = ""
keep_recording = True
recording_window = []
waiting_for_recording_input = False

date = ""
patient_name = ""
birthday = ""
age = ""
sex = ""
height = ""

anamnesis = "Anamnese:\nKeine Auffälligkeiten"
perception_result = "RBH-Skala:\nNicht durchgeführt\n"
egg_result = "EGG:\nNicht durchgeführt\n"
aerodynamics_result = "Aerodynamische Messungen:\nNicht durchgeführt\n"
acoustics_result = "Akustik:\nNicht durchgeführt\n"
vdi_result = "SSI:\nNicht durchgeführt\n"

patient_dir = "\\patient info"
wav_dir = "\\wav-files"

selected_speaker_name = ""
selected_mic_name = ""

session_saved = False

@QtCore.Slot(str)
def receive_patient_info(info_string):
    """
    Receives patient info from tab "Home" and edits it to process them 
    in Data_handler. Calls upon function "create_patient_file"
    to create file.

    Parameters:
    ----------
    info_string
            patient information

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global date
    global patient_name
    global birthday
    global age
    global sex
    global height

    info_string_splitted = info_string.split(",")
    date = info_string_splitted[0]
    patient_name = info_string_splitted[1]
    birthday = info_string_splitted[2]
    age = info_string_splitted[3]
    sex = info_string_splitted[4]
    height = info_string_splitted[5]

    file_name = create_patient_file()

@QtCore.Slot(str)
def receive_anamnesis(anamnesis_string):
    """
    Receives anamnesis information from tab "Home".

    Parameters:
    -----------
    anamnesis_string
            anamnesis information

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global anamnesis
    global session_saved
    session_saved = False
    anamnesis = anamnesis_string

@QtCore.Slot(str)
def receive_perception_result(result_string):
    """
    Receives perception results from tab "RBH-Skala".

    Parameters:
    -----------
    result_string
            results from tab "RBH-Skala"

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global perception_result
    global session_saved
    session_saved = False
    perception_result = result_string

@QtCore.Slot(str)
def receive_egg_result(result_string):
    """
    Receives egg results from tab "Elektroglottographie".

    Parameters:
    -----------
    result_string
            results from tab "Elektroglottographie"

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global egg_result
    global session_saved
    session_saved = False
    egg_result = result_string

@QtCore.Slot(str)
def receive_aerodynamics_result(result_string):
    """
    Receives aerodynamic results from tab "Aerodynamik".

    Parameters:
    -----------
    result_string
            results from tab "Aerodynamik"

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global aerodynamics_result
    global session_saved
    session_saved = False
    aerodynamics_result = result_string

@QtCore.Slot(str)
def receive_acoustics_result(result_string):
    """
    Receives acoustic results from tab "Akustik".

    Parameters:
    -----------
    result_string
            results from tab "Akustik"

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global acoustics_result
    global session_saved
    session_saved = False
    acoustics_result = result_string

@QtCore.Slot(str)
def receive_VDI_result(result_string):
    """
    Receives VDI results from tab "Stimmstörungsindex".

    Parameters:
    -----------
    result_string
            results from tab "Stimmstörungsindex"

    Called upon in constructor of class "Tab_widget" in "Main.py".
    """
    global vdi_result
    global session_saved
    session_saved = False
    vdi_result = result_string

@QtCore.Slot(str)
def receive_speaker_name(speaker_name):
    """
    Receives speakers name from menu "Audiogeräte" and "Lautsprecher".

    Parameters:
    -----------
    speaker_name
            name of clicked speaker

    Called upon in constructor of class "Main_window" in "Main.py".
    """
    global selected_speaker_name
    selected_speaker_name = speaker_name

@QtCore.Slot(str)
def receive_mic_name(mic_name):
    """
    Receives speakers name from menu "Audiogeräte" and "Mikrofon".

    Parameters:
    -----------
    mic_name
            name of clicked microphone

    Called upon in constructor of class "Main_window" in "Main.py".
    """
    global selected_mic_name
    selected_mic_name = mic_name

def get_signal():
    """
    Gets the signal for analysing. If signal is missing, the user is able
    to load a signal or to start a recording

    Return:
    -------
    fs  
            (float) sampling frequency of signal
    signal
            (array) signal to be analysed
    used_file_name
            (string) used file name to save in patient data

    Called upon in function "get_Lx" in "EGG.py" and in function "show_plots" in 
    "Acoustics.py".
    """
    global recording_window
    global waiting_for_recording_input
    global signal
    global fs
    global used_file_name

    if signal == []:
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIcon(QtWidgets.QMessageBox.Critical)
        msg_box.setWindowTitle("Signal benötigt!")
        msg_box.setText("Kein Signal gefunden!")
        msg_box.setInformativeText("Laden Sie entweder eine bereits vorhandene Datei oder nehmen Sie eine neue auf.")
        load_button = QtWidgets.QPushButton("Datei laden")
        msg_box.addButton(load_button, QtWidgets.QMessageBox.ActionRole)
        record_button = QtWidgets.QPushButton("Datei aufnehmen")
        msg_box.addButton(record_button, QtWidgets.QMessageBox.ActionRole)
        
        load_button.clicked.connect(load_file)
        record_button.clicked.connect(open_recording_window)

        msg_box.exec_()

    return fs, signal, used_file_name

def load_file():
    """
    Gets path from function and open file manager. Reads the selected .wav data
    and saves the used file name when button "Datei laden" is clicked.

    Called upon in function "get_signal" and constructor of class "Main_window" 
    in "Main.py".
    """
    global signal
    global fs
    global used_file_name
 
    path = get_destination_path(wav_dir)
    file, filter = QtWidgets.QFileDialog.getOpenFileName(None, "Datei öffnen", path, "Wav Files (*.wav)" )
    
    fs, signal =  wavfile.read(file)
    used_file_name = os.path.basename(file)
     
def open_recording_window():
    """
    Creates an objects of class "Recording_window", when signal is
    empty and button "Datei aufnehmen" is clicked.

    Called upon in function "get_signal" and constructor of class "Main_window"
    in "Main.py"
    """
    global recording_window
    global waiting_for_recording_input
    global signal

    signal = []
    waiting_for_recording_input = True
    recording_window = Recording_window() 
    recording_window.show()     

def start_recording():
    """
    Starts a thread to record a signal, when button "Aufnahme starten" 
    is clicked.

    Called upon in constructor of class "Recording_window".
    """
    recording_window.start_recording_button.setDisabled(True)
    recording_window.start_recording_button.setStyleSheet('background-color: lightgrey')
    recording_window.stop_recording_button.setDisabled(False)
    recording_window.stop_recording_button.setStyleSheet('background-color: red')
    recording_window.label_status.setText("Aufnahme läuft...")
    
    threading.Thread(target = record_within_thread, daemon=True).start()

def record_within_thread():
    """
    Sets all variables for recording. Records a signal and saves it by
    calling the function "save_recording_to_file".

    Called upon in function "start_recording".
    """
    global keep_recording
    global signal
    global fs
    global selected_mic_name
    
    # sets maximum recording time and block size
    max_rec_time = 120 
    block_size = 1024
    recording_buffer = list(range(int(max_rec_time * fs / block_size)))
    keep_recording = True
    
    # code used from: https://pypi.org/project/SoundCard/) (Bastian Bechtold)
    # last called: 01.06.22
    # get selected microphone
    mic = soundcard.default_microphone()
    if selected_mic_name != "":
        mic = soundcard.get_microphone(selected_mic_name)

    # saves data in buffer
    with mic.recorder(samplerate=fs) as mic: 
    
        loop_index = 0
        while keep_recording == True and loop_index < len(recording_buffer):
            print("recording in process")
            data_in = mic.record(numframes=block_size)
            data_buffer = data_in.astype(np.float32)
            recording_buffer[loop_index] = data_buffer
            loop_index += 1

        # closes recording window when maximum time is passed
        if loop_index >= len(recording_buffer):
            recording_window.close()

        # add all buffers to one variable
        data_out = []
        for idx in range(0, loop_index):
            data_buffer = recording_buffer[idx]
            data_out.extend(data_buffer)

        # saves data as numpy array and as file
        signal = np.asarray(data_out)
        save_recording_to_file(signal)

def stop_recording():
    """
    Closes the recording Window when button "Aufnahme beenden"
    is clicked.

    Called upon in constructor of class "Recording_window".
    """

    global keep_recording
    global waiting_for_recording_input

    keep_recording = False
    print("finished recording")
    recording_window.close()
    waiting_for_recording_input = False

def save_recording_to_file(signal):
    """
    Saves recording as a .wav file with patient name and 
    date and time of recording to directory "wav-files".
    
    Parameter:
    ----------
    signal
            (array) recorded signal

    Called upon in function "record_within_thread".
    """
    global fs
    global wav_dir
    global used_file_name
    global patient_name

    patient_name_splitted = patient_name.split(" ")
    patient_ = patient_name_splitted[0] + "_" + patient_name_splitted[-1]

    file_name = patient_ + "_" + datetime.now().strftime("%d-%m-%Y_%H-%M") + ".wav"

    destination_path = get_destination_path(wav_dir)
    complete_name = os.path.join(destination_path, file_name)
    wavfile.write(complete_name, fs, signal)    

    used_file_name = os.path.basename(file_name)

def save_all_data_to_file():
    """
    Saves results from all tabs in patients .txt file.
    
    Called upon in constructor and in function
    "save_upon_closing" of class "Main_window" in "Main.py". 
    """
    global session_saved

    global anamnesis
    global perception_result
    global egg_result
    global aerodynamics_result
    global acoustics_result
    global vdi_result

    # create patient file
    file_name = create_patient_file()

    # reads data from patient file
    with open(file_name, 'r') as result_file:
        temp_data_from_file = result_file.read()

    # print data that was previously in file already and
    # all new results
    with open(file_name, 'w') as result_file:
        print(temp_data_from_file, file = result_file)
        print("\n" + datetime.now().strftime("%d.%m.%Y %H:%M") + " Uhr\n", file = result_file)
        print(anamnesis + "\n", file = result_file)
        print(perception_result, file = result_file)
        print(egg_result, file = result_file)
        print(aerodynamics_result, file = result_file)
        print(acoustics_result, file = result_file)
        print(vdi_result, file = result_file)

    session_saved = True

def create_patient_file():
    """
    Creates patient .txt file with patient information to directory 
    "patient info".

    Return:
    -------
    complete_name
            (string) path in which patient file is saved to

    Called upon in function "save_all_data_to_file".
    """
    global patient_dir

    global patient_name
    global birthday
    global age
    global sex
    global height

    patient_name_splitted = patient_name.split(" ")
    file_name = patient_name_splitted[0]
    for name in patient_name_splitted[1:]:
        file_name = file_name + "_" + name
    file_name = file_name + ".txt"

    # gets path from directory
    destination_path = get_destination_path(patient_dir)
    complete_name = os.path.join(destination_path, file_name)
    
    # creates file with patient info if it does not already exists
    if os.path.exists(complete_name) == False:
        with open(complete_name, 'w') as result_file:
            header = f"Name: {patient_name}\n" + f"Geburtstag: {birthday}\n" + f"Geschlecht: {sex}\n" + f"Körpergröße: {height} cm\n\n" 
            print(header, file = result_file)

    return complete_name

def get_destination_path(directory):
    """
    Gets destination path of sound file or patient file in directory.

    Parameter:
    ----------
    directory
            (string) path of directory

    Return:
    -------
    destination_path
            (string) path of destination to save either sound file
            or patient file

    Called upon in function "create_patient_file" and 
    "save_recording_to_file".
    """
    # gets current path
    current_path = os.path.dirname(os.path.abspath(__file__))
    working_path = os.path.split(current_path)
    working_path = working_path[0]
    destination_path = working_path + directory

    # tries to create directory if it does not already exists
    try:
        os.makedirs(destination_path)
    except FileExistsError:
        pass

    return destination_path  

def play_audio():
    """
    Plays the recorded or loaded signal.

    Called upon in constructor of class "Main_window" in "Main.py".
    """
    global signal
    global selected_speaker_name

    # code used from: https://pypi.org/project/SoundCard/) (Bastian Bechtold)
    # last called: 01.06.22
    speaker = soundcard.default_speaker() 
    if selected_speaker_name != "":
        speaker = soundcard.get_speaker(selected_speaker_name)
    
    channel_1 = signal[:,0]
    speaker.play(channel_1, fs, channels=1)

class Recording_window(QtWidgets.QWidget):
    def __init__(self):
        """
        Constructor of class "Recording_window".

        Creates a widget to control the recording. Start and stop the
        recording via buttons.

        Called upon in function "open_recording_window".
        """
        super().__init__()

        self.setWindowTitle("Aufnahme")
        self.setWindowIcon(QtGui.QIcon("program\\icons\\microphone.png"))

        info_label = QtWidgets.QLabel("Bedienen Sie die Knöpfe um eine Aufnahme zu beginnen und zu beenden.\nBitte achten Sie auf richtige Positionierung der Elektroden und wählen Sie die gewünschten Audiogeräte im Menüreiter 'Audiogeräte' aus.")
        self.start_recording_button = QtWidgets.QPushButton("Aufnahme starten")
        self.start_recording_button.setStyleSheet('background-color: red')
        self.stop_recording_button = QtWidgets.QPushButton("Aufnahme beenden")
        self.stop_recording_button.setDisabled(True)

        self.start_recording_button.clicked.connect(start_recording)
        self.stop_recording_button.clicked.connect(stop_recording)

        self.label_status = QtWidgets.QLabel("")

        layout = QtWidgets.QFormLayout()
        layout.addRow(info_label)
        layout.addRow(self.start_recording_button)
        layout.addRow(self.stop_recording_button)
        layout.addRow(self.label_status)
        self.setLayout(layout)